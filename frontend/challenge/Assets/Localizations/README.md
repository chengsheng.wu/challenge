# Localization
- Contains font, text localization. Support localization before patch and in game. 
- Currently didn't handle font localization due to time constraint. What I did in my previous project
is 
    - Before patch, using Unity Text instead of TMP due to Unity Text support system font fallback but TMP doesn't and to reduce apk size.
    - In game, language supported may have different static font asset(ex: zh-cn, zh-tw, ko-kr, ja-jp) and load base on language selected, a dynamic font asset that support all language and set as default fall back at runtime.

## Folder structure
- ColorPreset: store predefined color gradient to be used by `TextMeshPro-Text`
- Csv: contain localized `key/value pairs` for different languages, csv files are `exported from excel` by designer in real development
- Data: store `generated scriptobjects` by running `Tools/Localization/Build Localization Container Data` 
- Font: store `font assets` and `material presets`. 
- Resources: store localizations used `before patch`, they are `built into apk`.  
- Setting: Config in game supported languages in `LanguageSetting.asset` 
- TTF: contains [True Type Font](https://www.techopedia.com/definition/12522/truetype-font-ttf). Both static/dynamic font assets are created from ttf.
- The localization scripts are under `Scripts/Localization/` 
    - `editor script` to generate scriptobject from csv 
    - `LocalizedText` component to display text with localization.
    - `Text and Font managers`

## Steps to support new language
- Duplicate `Csv/en/` and `rename` `en` to your language shortcut {xx}.
- Update all the `localization values` to your language.
- Regenerate new language data from `Tools/Localization/Build Localization Container Data`.
- Verify 
    - localizations with apk are created under `Localizations/Resources/{xx}` 
    - localizations in game are created under `Localizations/Data/{xx}`
- Click `Localizations/Resources/LanguageSetting`, Add new language to `Supported Languages` from its `inspector` so that `before patch` it can display correct localization for new language. This required new apk to take effect.
- Click `Localizations/Setting/LanguageSetting`, Add new language to `Supported Languages` from its `inspector` so that `in game` it can display correct localization for new language. This can be patched by asset bundle.