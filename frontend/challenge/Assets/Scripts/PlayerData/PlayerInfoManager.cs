﻿using Common;

namespace PlayerData
{
    public class PlayerData
    {
        public int id;
        public string name;
        public int profileIconId;

        public PlayerData(int id, string name, int profileIconId)
        {
            this.id = id;
            this.name = name;
            this.profileIconId = profileIconId;
        }
    }
    
    public class PlayerInfoManager : SingletonBase<PlayerInfoManager>
    {
        private PlayerData myPlayerData;
        private PlayerData opponentPlayerData;

        public void Init()
        {
            myPlayerData = new PlayerData(1, "Player1", 1);
            opponentPlayerData = new PlayerData(2, "Player2", 2);
            PlayerDeckManager.Instance.Init();
        }

        public PlayerData GetPlayerData()
        {
            return myPlayerData;
        }

        public PlayerData GetOpponentPlayerData()
        {
            return opponentPlayerData;
        }
    }
}