﻿using System.Collections.Generic;

namespace PlayerData.Tournament
{
    public enum MatchMakingStatus
    {
        Waiting,
        Joined,
        Ready,
    }

    public enum MatchMakingStage
    {
        Prepare,
        Ban,
        BanEnd,
        Pick,
        PickEnd,
        MatchStarting,
    }
    
    public class TournamentPlayerData
    {
        private PlayerData player;
        private List<DeckData> deckDatas;

        public TournamentPlayerData(PlayerData player, List<DeckData> deckDatas)
        {
            this.player = player;
            this.Status = MatchMakingStatus.Joined;
            this.deckDatas = deckDatas;
        }

        public PlayerData GetPlayerData()
        {
            return player;
        }

        public MatchMakingStatus Status { get; set; }

        public List<DeckData> GetDecks()
        {
            return deckDatas;
        }
    }
}