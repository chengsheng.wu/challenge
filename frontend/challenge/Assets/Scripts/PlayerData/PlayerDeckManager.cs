﻿using System.Collections.Generic;
using System.Linq;
using Common;
using GameData;
using Managers;

public class DeckData
{
    public int ID { get; }
    public string Name { get; set; }
    public string IconName { get; set; }
    public List<int> CardIds { get; }

    public DeckData(int id, string name, string iconName, List<int> cardIds)
    {
        ID = id;
        Name = name;
        IconName = iconName;
        CardIds = cardIds ?? new List<int>();
    }

    public Dictionary<CardRegion, bool> GetRegions()
    {
        var regions = new Dictionary<CardRegion, bool>();
        var gameDataManager = GameDataManager.Instance;
        foreach (var cardId in CardIds)
        {
            regions[gameDataManager.GetCardInfoById(cardId).region] = true;
        }

        return regions;
    }

    public bool HasRegion(CardRegion region)
    {
        var gameDataManager = GameDataManager.Instance;
        return CardIds.Any(cardId => gameDataManager.GetCardInfoById(cardId).region == region);
    }

    public List<int> GetUniqueHeroCardIds()
    {
        var heroIds = new Dictionary<int, bool>();
        var gameDataManager = GameDataManager.Instance;
        foreach (var cardId in CardIds)
        {
            if (!gameDataManager.GetCardInfoById(cardId).hero)
                continue;

            heroIds[cardId] = true;
        }

        if (heroIds.Count == 0)
            return new List<int>();
        
        return heroIds.Select(kvp => kvp.Key).ToList();
    }

    public bool IsEmpty()
    {
        return CardIds.Count == 0;
    }
}

public class PlayerDeckManager : SingletonBase<PlayerDeckManager>
{
    // by right, both player decks and opponentDecks are from server,
    // since we have no real server in this challenge, just hardcode here for later usage.
    private readonly Dictionary<int, DeckData> playerDecks = new Dictionary<int, DeckData>();
    private readonly Dictionary<int, DeckData> opponentDecks = new Dictionary<int, DeckData>();

    public void Init()
    {
        BuildPlayerDeck();
        BuildOppDeck();
    }

    public Dictionary<int, DeckData> GetPlayerDecks()
    {
        return playerDecks;
    }

    public DeckData GetPlayerDeckById(int deckId)
    {
        return playerDecks.TryGetValue(deckId, out var deckData) ? deckData : null;
    }

    public Dictionary<int, DeckData> GetOpponentDecks()
    {
        return opponentDecks;
    }

    public List<DeckData> GetOpponentDeckList()
    {
        return opponentDecks.Select(kvp => kvp.Value).ToList();
    }
    
    public DeckData GetOpponentDeckById(int deckId)
    {
        return opponentDecks.TryGetValue(deckId, out var deckData) ? deckData : null;
    }

    private void BuildPlayerDeck()
    {
        //the first letter in deck name is for test sort by Alphabetical in select deck window
        
        // test case with 17 cards total, 9 unique cards,
        // hero 1(3), hero 4(1), hero 10(2)
        // region 1(card 10), 2(card 7)
        var deck = new DeckData(1, "b_deck_1", "DeckIcons/deck_bg_1", 
            new List<int>{1,1,1,2,2,3,3,4,6,6,6,10,10,11,11,12,14});
        playerDecks[deck.ID] = deck;
        
        // test case with 17 cards total, 7 unique cards,
        // hero 1(3), hero 9(3)
        // region 1(card 11), 4(card 6)
        deck = new DeckData(2, "a_deck_2", "DeckIcons/deck_bg_2", 
            new List<int>{1,1,1,2,2,8,8,8,9,9,9,11,11,11,12,12,14});
        playerDecks[deck.ID] = deck;
        
        // test case with 4 cards total, 4 unique cards,
        // hero 1(1), hero 4(1)
        // region 1(card 3), 2(card 1)
        deck = new DeckData(3, "d_deck_3", "DeckIcons/deck_bg_3", 
            new List<int>{1,2,3,4});
        playerDecks[deck.ID] = deck;

        // test case with card id not in order
        // test case with 4 cards total, 4 unique cards,
        // hero 1(1), hero 4(1)
        // region 1(card 3), 2(card 1)
        deck = new DeckData(4, "c_deck_4", "DeckIcons/deck_bg_4", 
            new List<int>{3,1,4,2});
        playerDecks[deck.ID] = deck;
        
        // test case with 9 cards total, 5 unique cards,
        // hero 1(1), hero 4(1), hero 5(3)
        // region 1(card 3), 3(card 6)
        deck = new DeckData(5, "deck_5", "DeckIcons/deck_bg_5", 
            new List<int>{1,2,4,5,5,5,7,7,7});
        playerDecks[deck.ID] = deck;
        
        // test case with no hero card
        // region 3(card 3)
        deck = new DeckData(6, "deck_6", "DeckIcons/deck_bg_6", 
            new List<int>{7,7,7});
        playerDecks[deck.ID] = deck;
        
        // test case with 4 unique heroes to see whether display correct in Ban and Pick
        // test case with 17 cards total, 10 unique cards,
        // hero 1(2), hero 4(1), hero 10(2), hero 13(1)
        // region 1(card 10), 2(card 7)
        deck = new DeckData(7, "b_deck_7", "DeckIcons/deck_bg_7", 
            new List<int>{1,1,2,2,3,3,4,6,6,6,10,10,11,11,12,13,14});
        playerDecks[deck.ID] = deck;
        
        //edge case that deck has 0 card inside
        //assume deck with 0 card won't be allowed bring to battle, so it shouldn't be populated in decks selection window
        deck = new DeckData(8, "deck_8", "DeckIcons/deck_bg_8", 
            new List<int>());
        playerDecks[deck.ID] = deck;
    }

    private void BuildOppDeck()
    {
        // To save time this deck is same as player deck 1
        // test case with 17 cards total, 7 unique cards,
        // hero 1(3), hero 9(3)
        // region 1(card 11), 4(card 6)
        var deck = new DeckData(1, "deck_1", "DeckIcons/deck_bg_1", 
            new List<int>{1,1,1,2,2,3,3,4,6,6,6,10,10,11,11,12,14});
        opponentDecks[deck.ID] = deck;
        
        // To save time this deck is same as player deck 2
        // test case with 17 cards total, 7 unique cards,
        // hero 1(3), hero 9(3)
        // region 1(card 11), 4(card 6)
        deck = new DeckData(2, "deck_2", "DeckIcons/deck_bg_2", 
            new List<int>{1,1,1,2,2,8,8,8,9,9,9,11,11,11,12,12,14});
        opponentDecks[deck.ID] = deck;
        
        // To save time this deck is same as player deck 5
        // test case with 9 cards total, 5 unique cards,
        // hero 1(1), hero 4(1), hero 5(3)
        // region 1(card 3), 3(card 6)
        deck = new DeckData(3, "deck_3", "DeckIcons/deck_bg_3", 
            new List<int>{1,2,4,5,5,5,7,7,7});
        opponentDecks[deck.ID] = deck;
    }
}