﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;
using UnityEngine.Video;
using Object = UnityEngine.Object;

namespace Managers
{
    public class ResourceManager : SingletonBase<ResourceManager>
    {
        private readonly Dictionary<string, GameObject> preloadedPrefabs = new Dictionary<string, GameObject>();
        private readonly Dictionary<string, SpriteAtlas> preloadedSpriteAtlas = new Dictionary<string, SpriteAtlas>();
        private readonly Dictionary<string, Dictionary<string, Sprite>> cachedSprites = new Dictionary<string, Dictionary<string, Sprite>>();
        private readonly List<AsyncOperationHandle> handles = new List<AsyncOperationHandle>();
        
        public IEnumerator Preload()
        {
            SpriteAtlasManager.atlasRequested += RequestAtlas;
            yield return InternalPreloadSprite();
            yield return InternalPreloadPrefab();
        }

        public void Cleanup()
        {
            preloadedPrefabs.Clear();
            preloadedSpriteAtlas.Clear();
            cachedSprites.Clear();
            SpriteAtlasManager.atlasRequested -= RequestAtlas;
            foreach (var handle in handles)
            {
                Addressables.Release(handle);
            }
            handles.Clear();
        }
        
        void RequestAtlas(string tag, Action<SpriteAtlas> callback)
        {
            var atlas = GetAtlas(tag);
            callback(atlas);
            if (atlas == null)
            {
                LogUtil.WarningFormat("RequestAtlas tag = {0} missing", tag);       
            }
        }

        private IEnumerator InternalPreloadSprite()
        {
            var handler = Addressables.LoadAssetsAsync<SpriteAtlas>((IEnumerable)new List<object> {"preload", "sprite"}, null,
                Addressables.MergeMode.Intersection);

            yield return handler;
            foreach (var atlas in handler.Result)
            {
                if(preloadedSpriteAtlas.ContainsKey(atlas.name))
                {
                    throw new SystemException("Duplicated Sprite Atlas Name");
                }

                cachedSprites.Add(atlas.name, new Dictionary<string, Sprite>());
                preloadedSpriteAtlas.Add(atlas.name, atlas);
            }
            handles.Add(handler);
            LogUtil.Info("InternalPreloadSprite Done");
        }
        
        private IEnumerator InternalPreloadPrefab()
        {
            #pragma warning disable 0612
            var handler = Addressables.LoadAssetsAsync<GameObject>(new List<object>{"preload", "prefab"},null, Addressables.MergeMode.Intersection);
            #pragma warning restore 0612
            yield return handler;
            if (handler.Status == AsyncOperationStatus.Succeeded)
            {
                foreach (var go in handler.Result)
                {
                    if (preloadedPrefabs.ContainsKey(go.name))
                    {
                        throw new SystemException("Duplicated Prefab Name");
                    }

                    preloadedPrefabs.Add(go.name, go);
                }

                handles.Add(handler);
            }
            else
            {
                Addressables.Release(handler);
            }
        }

        private SpriteAtlas GetAtlas(string atlasName)
        {
            return preloadedSpriteAtlas.TryGetValue(atlasName, out var spritesInAtlas) ? spritesInAtlas : null;
        }

        public Sprite LoadSprite(string atlasSprite)
        {
            string[] atlasAndSprite = atlasSprite.Split('/');
            if (atlasAndSprite.Length == 2)
                return LoadSprite(atlasAndSprite[0], atlasAndSprite[1]);
            LogUtil.WarningFormat("LoadSprite {0} is not atlas/sprite format", atlasSprite);
            return null;
        }
        
        public Sprite LoadSprite(string atlasName, string spriteName)
        {
            if (cachedSprites.TryGetValue(atlasName, out var spritesInAtlas))
            {
                if (!spritesInAtlas.TryGetValue(spriteName, out var sprite))
                {
                    sprite = preloadedSpriteAtlas[atlasName].GetSprite(spriteName);
                    spritesInAtlas.Add(spriteName, sprite);
                }

                return sprite;
            }
            return null;
        }
        
        public void LoadSpriteAsync(string path, Action<Sprite> onLoaded)
        {
            Addressables.LoadAssetAsync<Sprite>(path).Completed += handle =>
            {
                onLoaded?.Invoke(handle.Result);
            };
        }
        
        public void LoadAudioClipAsync(string address, Action<AudioClip> onLoaded)
        {
            Addressables.LoadAssetAsync<AudioClip>(address).Completed += handle =>
            {
                onLoaded?.Invoke(handle.Result);
            };
        }
        
        public void LoadVideoClipAsync(string address, Action<VideoClip> onLoaded)
        {
            Addressables.LoadAssetAsync<VideoClip>(address).Completed += handle =>
            {
                onLoaded?.Invoke(handle.Result);
            };
        }
        
        public void LoadSpriteAtlasAsync(string path, Action<SpriteAtlas> onLoaded)
        {
            Addressables.LoadAssetAsync<SpriteAtlas>(path).Completed += handle =>
            {
                onLoaded?.Invoke(handle.Result);
            };
        }

        public AsyncOperationHandle<GameObject> InstantiateGameObjectAsync(string address, Transform parent, Vector3 position, Action<GameObject> onComplete)
        {
            var rotation = parent == null ? Quaternion.identity : parent.rotation;
            var handle = Addressables.InstantiateAsync(address, position, rotation, parent);
            handle.Completed += op =>
            {
                onComplete?.Invoke(op.Result);
                onComplete = null;
            };
            return handle;
        }

        public void InstantiateGameObjectAsync_Overload1(string address, Transform parent, Action<GameObject> onComplete)
        {
            Addressables.InstantiateAsync(address, parent).Completed += op =>
            {
                op.Result.transform.SetParent(parent);
                onComplete?.Invoke(op.Result);
                onComplete = null;
            };
        }

        public GameObject InstantiatePreloadGameObject(string address, Transform parent, Vector3 position)
        {
            if (preloadedPrefabs.TryGetValue(address, out var go))
            {
                var gameObject = Object.Instantiate(go, parent, false);
                gameObject.transform.localPosition = position;
                return gameObject;
            }
            
            LogUtil.Error("Preloaded Asset Not Found:" + address);
            return null;
        }
        
        public void ReleaseInstance(GameObject go)
        {
            var success = Addressables.ReleaseInstance(go);
            if (!success)
            {
                Object.Destroy(go);
            }
        }
        
        public void ReleaseAudioClip(AudioClip s)
        {
            if (s != null)
            {
                Addressables.Release(s);
            }
        }
        
        public void ReleaseVideoClip(VideoClip c)
        {
            if (c != null)
            {
                Addressables.Release(c);
            }
        }

        public void ReleaseSprite(Sprite s)
        {
            if (s != null)
            {
                Addressables.Release(s);
            }
        }
        
        public void ReleaseSpriteAtlas(SpriteAtlas s)
        {
            if (s != null)
            {
                Addressables.Release(s);
            }
        }
    }
}
