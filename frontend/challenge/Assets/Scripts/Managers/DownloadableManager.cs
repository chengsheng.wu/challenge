﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Managers
{
    public class DownloadableManager : SingletonBase<DownloadableManager>
    {
        private readonly Dictionary<string, IList<IResourceLocation>> _cachedDlcLocations;
        private readonly Dictionary<string, AsyncOperationHandle> _currentDownloadignDlc;
        private readonly List<string> _downloadedDlc;
        private readonly AsyncOperationHandle dummyHandle;

        public DownloadableManager()
        {
            _cachedDlcLocations = new Dictionary<string, IList<IResourceLocation>>();
            _currentDownloadignDlc = new Dictionary<string, AsyncOperationHandle>();
            _downloadedDlc = new List<string>();
            dummyHandle = new AsyncOperationHandle();
        }

        public void Cleanup()
        {
            _cachedDlcLocations.Clear();
            _downloadedDlc.Clear();
            _currentDownloadignDlc.Clear();
        }

        public void GetDlcLocations(string dlcType, string dlcName, Action<IList<IResourceLocation>> onComplete)
        {
            CoroutineHelper.Instance.StartCoroutine(InternalGetDlcLocations(dlcType, dlcName, onComplete));
        }

        public void GetDlcDownloadSize(string dlcType, string dlcName, Action<float> onComplete)
        {
            CoroutineHelper.Instance.StartCoroutine(InternalGetDownloadSize(dlcType, dlcName, onComplete));
        }

        public void DownloadDlcContent(string dlcType, string dlcName, Action onComplete)
        {
            CoroutineHelper.Instance.StartCoroutine(InternalDownloadDlc(dlcType, dlcName, onComplete));
        }

        private IEnumerator InternalGetDlcLocations(string dlcType, string dlcName,
            Action<IList<IResourceLocation>> onComplete)
        {
            //make search key
            var key = MakeSearchKey(dlcType, dlcName);

            //try get locations from cache
            if (_cachedDlcLocations.TryGetValue(key, out var locations))
            {
                onComplete?.Invoke(locations);
                yield break;
            }

            //if not found in cache, then start load locations
            #pragma warning disable 0612

            var locationHandle = Addressables.LoadResourceLocationsAsync(new List<object> {dlcType, dlcName},
                Addressables.MergeMode.Intersection);
            #pragma warning restore 0612
            
            yield return locationHandle;

            //add to cached locations
            _cachedDlcLocations.Add(key, locationHandle.Result);

            //callback
            onComplete?.Invoke(locationHandle.Result);

            //release handle
            Addressables.Release(locationHandle);
        }

        private IEnumerator InternalDownloadDlc(string dlcType, string dlcName, Action onComplete)
        {
            //make search key
            var searchKey = MakeSearchKey(dlcType, dlcName);

            //if dlc is downloading, return, dont callback in case there is duplicated callback
            if (_currentDownloadignDlc.ContainsKey(searchKey))
            {
                LogUtil.Error("Dlc is downloading ");
                yield break;
            }

            //if dlc is downloaded, callback
            if (_downloadedDlc.Contains(searchKey))
            {
                onComplete?.Invoke();
                yield break;
            }

            //set downloading flag (using dummy handle)
            _currentDownloadignDlc.Add(searchKey, dummyHandle);

            //get dlc locations
            IList<IResourceLocation> locations = new List<IResourceLocation>();
            var waitForLocation = true;
            yield return InternalGetDlcLocations(dlcType, dlcName, list =>
            {
                locations = list;
                waitForLocation = false;
            });
            while (waitForLocation) yield return null;

            //start download dlc
            var downloadHandle = Addressables.DownloadDependenciesAsync(locations);

            //set downloading flag (using actual handle)
            _currentDownloadignDlc[searchKey] = downloadHandle;

            //wait for download complete
            yield return downloadHandle;

            //remove downloading flag
            _currentDownloadignDlc.Remove(searchKey);

            //set downloaded flag
            _downloadedDlc.Add(searchKey);

            //callback
            onComplete?.Invoke();

            //release handle
            Addressables.Release(downloadHandle);
        }

        private IEnumerator InternalGetDownloadSize(string dlcType, string dlcName, Action<float> onComplete)
        {
            //make search key
            var key = MakeSearchKey(dlcType, dlcName);

            //get locations
            IList<IResourceLocation> locations = new List<IResourceLocation>();
            var waitForLocation = true;
            yield return InternalGetDlcLocations(dlcType, dlcName, list =>
            {
                locations = list;
                waitForLocation = false;
            });
            while (waitForLocation) yield return null;

            //get dlc download size
            var sizeHandle = Addressables.GetDownloadSizeAsync(locations);
            yield return sizeHandle;

            //if dlc is downloaded, set downloaded flag
            if (sizeHandle.Result <= 0)
            {
                if (!_downloadedDlc.Contains(key)) _downloadedDlc.Add(key);
            }

            //convert to mb
            var sizeInMb = GameUtils.Instance.ConvertBytesToMegabytes(sizeHandle.Result);

            //call back
            onComplete?.Invoke((float) sizeInMb);

            //release handle
            Addressables.Release(sizeHandle);
        }

        private string MakeSearchKey(string dlcType, string dlcName)
        {
            return $"{dlcType}@{dlcName}";
        }
    }
}