﻿using System.Collections.Generic;
using Common;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Managers
{
    public class InputManager : MonoSingletonBase<InputManager>
    {
        private EventSystem eventSystem;
        private PointerEventData eventDataCurrentPosition;
        private Vector2 mouseClickPosition = Vector2.zero;
        private readonly List<RaycastResult> results = new List<RaycastResult>();

        public void Init()
        {
            eventSystem = EventSystem.current;
            eventDataCurrentPosition = new PointerEventData(eventSystem);
        }

        public void Cleanup()
        {
            eventSystem = null;
            eventDataCurrentPosition = null;
            results.Clear();
        }

        public bool IsPointerOverGameObject()
        {
            mouseClickPosition.x = Input.mousePosition.x;
            mouseClickPosition.y = Input.mousePosition.y;
            eventDataCurrentPosition.position = mouseClickPosition;
            results.Clear();
            eventSystem.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}
