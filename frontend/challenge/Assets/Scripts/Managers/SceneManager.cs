﻿using System.Collections;
using Common;
using UI;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Managers
{
    public class SceneManager : MonoSingletonBase<SceneManager>
    {
        private bool isLoading = false;
        private string currentSceneName = "";
        
        public float Progress { get; set; }
        
        public void SwitchScene(string sceneName)
        {
            if (isLoading || currentSceneName == sceneName)
                return;
            
            UIManager.Instance.OpenWindow(WindowType.LoadingWindow, null);
            StartCoroutine(CO_SwitchScene(sceneName));
        }

        private IEnumerator CO_SwitchScene(string sceneName)
        {
            Progress = 0.1f;
            isLoading = true;
            float start = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(0.1f);

            UIManager.Instance.CloseAllWindowByShowMode(WindowShowMode.Normal);
            Progress = 0.2f;
            yield return new WaitForSeconds(0.1f);
            
            var clearOperation = UnityEngine.Resources.UnloadUnusedAssets();
            while (!clearOperation.isDone)
            {
                Progress = 0.2f + 0.4f * clearOperation.progress;
                yield return new WaitForSeconds(0.1f);
            }
            
            yield return new WaitForSeconds(0.5f);
            
            var sceneOperation = Addressables.LoadSceneAsync(string.Format("{0}.unity", sceneName));
            while (!sceneOperation.IsDone)
            {
                Progress = 0.6f + 0.4f * sceneOperation.PercentComplete;
                yield return new WaitForSeconds(0.1f);
            }

            float diff = Time.realtimeSinceStartup - start;
            if (diff < 2)
                yield return new WaitForSeconds(2-diff);

            UIManager.Instance.CloseWindow(WindowType.LoadingWindow);
            currentSceneName = sceneName;
            Progress = 0;
            isLoading = false;
        }
    }
}