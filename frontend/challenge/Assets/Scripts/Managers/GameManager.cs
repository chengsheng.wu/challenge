﻿using System;
using System.Collections;
using Common;
using DG.Tweening;
using Network;
using PlayerData;
using UnityEngine;

namespace Managers
{
    public enum GAME_STATE : byte
    {
        Splash,
        Game,
    }
    
    public class GameManager : MonoSingletonBase<GameManager>
    {
        [HideInInspector] public GAME_STATE gameState = GAME_STATE.Splash;
        private float lastRecordedTime = 0f;
        private static int fpsLimit;

        #region Start & Restart
        public void StartGame()
        {
            StartCoroutine(InternalStartGame());
        }

        public void RestartGame()
        {
            StartCoroutine(InternalRestartGame());
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private IEnumerator InternalCleanupGame()
        {
            // clear tween
            DOTween.KillAll(false);
            DOTween.Clear(true);
            DOTween.ClearCachedTweens();
            yield return new WaitForFixedUpdate();

            DownloadableManager.Instance.Cleanup();
            InputManager.Instance.Cleanup();
            ResourceManager.Instance.Cleanup();
            LocalizationManager.Instance.Cleanup();
            GameDataManager.Instance.Cleanup();
            yield return new WaitForFixedUpdate();
            
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private IEnumerator InternalStartGame()
        {
            gameState = GAME_STATE.Splash;
            LocalizationManager.Instance.SplashScreenPreload();

            SplashScreen.Instance.SetVersion();
            
            var textDisplay = LocalizationManager.Instance.GetLocalizedText("loading_resource");
            RecordTimeDuration("Game Started Up");
            
            //addressable content patching
            yield return PatchManager.Instance.StartPatch();
            RecordTimeDuration("Patch Completed");
            SplashScreen.Instance.SetProgress(textDisplay, .1f);

            //preload resources
            yield return ResourceManager.Instance.Preload();
            RecordTimeDuration("ResourceManager Preload Completed");
            SplashScreen.Instance.SetProgress(textDisplay, .3f);

            var uiCanvas = ResourceManager.Instance.InstantiatePreloadGameObject("Canvas", null, Vector3.zero);
            if (uiCanvas == null)
            {
                SplashScreen.Instance.Notify(LocalizationManager.Instance.GetLocalizedText("missing_canvas"), 
                    false, "", 
                    LocalizationManager.Instance.GetLocalizedText("quit"), 
                    null, QuitGame
                );
                //not allow to continue if canvas load fail.
                while (true)
                {
                    yield return null;
                }
            }
            RecordTimeDuration("Instantiate canvas Completed");
            SplashScreen.Instance.SetProgress(textDisplay, .5f);

            yield return LocalizationManager.Instance.Preload();
            RecordTimeDuration("LocalizationManager Preload Completed");
            SplashScreen.Instance.SetProgress(textDisplay, .6f);
            
            yield return GameDataManager.Instance.Preload();
            RecordTimeDuration("GameDataManager Preload Completed");
            SplashScreen.Instance.SetProgress(textDisplay, .8f);
            
            InputManager.Instance.Init();
            RecordTimeDuration("InputManager Init Completed");
            SplashScreen.Instance.SetProgress(textDisplay, 0.9f);
            
            // by right after login can get all player information from lobby server
            // the following code will print a timed out error because url not exists, please ignore this error message.
            var webRequest = NetworkHandler.Instance.GetWebRequest("https://localhost/lobby/status",
                data => PlayerInfoManager.Instance.Init(),
                data => PlayerInfoManager.Instance.Init());
            yield return webRequest;

            RecordTimeDuration("PlayerDeckManager Init Completed");
            SplashScreen.Instance.SetProgress(textDisplay, 1.0f);
            
            gameState = GAME_STATE.Game;
            SceneManager.Instance.SwitchScene("Main");
        }

        private IEnumerator InternalRestartGame()
        {
            yield return InternalCleanupGame();
            yield return InternalStartGame();
        }

        public void RecordTimeDuration(string content)
        {
            var now = Time.realtimeSinceStartup;
            var duration = now - lastRecordedTime;
            LogUtil.DebugFormat("[RecordTimeDuration] {0} Took {1} Seconds", content, duration);
            lastRecordedTime = now;
        }

        #endregion
    }
}
