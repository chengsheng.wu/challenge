﻿using System.Collections;
using System.Collections.Generic;
using Common;
using GameData;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Managers
{
    public class GameDataManager : SingletonBase<GameDataManager>
    {
        public Dictionary<int, CardInfo> cardInfos = new Dictionary<int, CardInfo>();
        public Dictionary<string, string> constantInfos = new Dictionary<string, string>();

        public IEnumerator Preload()
        {
            var handle = Addressables.LoadAssetsAsync<ScriptableObject>("game-data", null);
            yield return handle;
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                cardInfos.Clear();
                foreach (var container in handle.Result)
                {
                    (container as IGameDataContainer)?.Load(this);       
                }
            }
            Addressables.Release(handle);
        }

        public void Cleanup()
        {
            cardInfos.Clear();
        }

        public CardInfo GetCardInfoById(int id)
        {
            return cardInfos.TryGetValue(id, out var cardInfo) ? cardInfo : null;
        }

        public int GetConstantInt(string key, int defaultValue=0)
        {
            if (constantInfos.TryGetValue(key, out var value) && int.TryParse(value, out var result))
                return result;

            return defaultValue;
        }

        public string GetConstantString(string key, string defaultValue = "")
        {
            return constantInfos.TryGetValue(key, out var value) ? value : defaultValue;
        }
    }
}