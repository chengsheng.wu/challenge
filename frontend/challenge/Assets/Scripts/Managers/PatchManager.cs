﻿using System.Collections;
using System.Collections.Generic;
using Common;
using Managers;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class PatchManager : SingletonBase<PatchManager>
{
    private List<IResourceLocation> patchList = new List<IResourceLocation>();

    public IEnumerator StartPatch()
    {
        SplashScreen.Instance.SetProgress(LocalizationManager.Instance.GetLocalizedText("check_apk_version"), 1f);
        yield return Patch();
        SplashScreen.Instance.SetProgress(LocalizationManager.Instance.GetLocalizedText("patch_complete"), 1f);
    }

    private IEnumerator Patch()
    {
        yield return CheckServerMaintenanceAtStartUp();
        yield return CheckApkVersion();
        yield return SetRemoteBundleLoadPath();
        yield return UpdateCatalog();
        yield return UpdatePatchList();

        //get total download size
        var downloadSizeHandle = Addressables.GetDownloadSizeAsync(patchList);
        yield return downloadSizeHandle;
        
        var downloadSizeInMb = GameUtils.Instance.ConvertBytesToMegabytes(downloadSizeHandle.Result);
        Addressables.Release(downloadSizeHandle);
        
        //if user is using mobile data, confirm before proceed to download
        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork && downloadSizeInMb >= 5)
            yield return ConfirmBeforeProceedToDownload(downloadSizeInMb);

        //download asset by the download list
        yield return DownloadAsset(patchList, downloadSizeInMb);
        patchList.Clear();
    }

    private IEnumerator ConfirmBeforeProceedToDownload(double downloadSizeInMb)
    {
        //set display message
        var msgToDisplay = string.Format(LocalizationManager.Instance.GetLocalizedText("patch_confirmation"), downloadSizeInMb);
        var continueDownload = false;
        var waitingForInput = true;

        //notify users, waiting for them to decide whether to download or not
        SplashScreen.Instance.Notify(msgToDisplay, 
            false, "", 
            LocalizationManager.Instance.GetLocalizedText("ok"), 
            null, () =>
            {
                continueDownload = true;
                waitingForInput = false;
            }
        );
        
        while (waitingForInput) 
            yield return null;

        if (continueDownload == false) 
            GameManager.Instance.QuitGame();
    }

    private IEnumerator UpdatePatchList()
    {
        patchList.Clear();
        
        var currentLangCode = SplashScreen.Instance.GetCurrentLangCode();

        //get locations for built-in content, assets with built-in label will be patched after app launch
        var builtInLocationHandle = Addressables.LoadResourceLocationsAsync(Settings.BuiltInLabel);
        yield return builtInLocationHandle;
        patchList.AddRange(builtInLocationHandle.Result);
        Addressables.Release(builtInLocationHandle);

        //get locations of localization assets for current language only
        var waitForDlcLocation = true;
        DownloadableManager.Instance.GetDlcLocations(Settings.LocalizationLabel, currentLangCode, list =>
        {
            patchList.AddRange(list);
            waitForDlcLocation = false;
        });
        
        while (waitForDlcLocation) 
            yield return null;
    }

    private IEnumerator DownloadAsset(IList<IResourceLocation> locations, double downloadSizeInMb)
    {
        var sizeInMb = downloadSizeInMb.ToString("F");
        LogUtil.Info($"Total Download Size {sizeInMb} MB", "cyan");
        
        if (downloadSizeInMb > 0)
        {
            //start download
            var downloadHandle = Addressables.DownloadDependenciesAsync(locations);

            //wait and update splash screen
            while (!downloadHandle.IsDone)
            {
                var downloadStatus = downloadHandle.GetDownloadStatus();
                var displayText = string.Format(LocalizationManager.Instance.GetLocalizedText("patch_size"), 
                    GameUtils.Instance.ConvertBytesToMegabytes(downloadStatus.DownloadedBytes), sizeInMb);
                SplashScreen.Instance.SetProgress(displayText, downloadStatus.Percent);
                yield return null;
            }

            var status = downloadHandle.Status;
            Addressables.Release(downloadHandle);
            
            if (status == AsyncOperationStatus.Failed)
            {
                bool waitRetry = true;
                SplashScreen.Instance.Notify(LocalizationManager.Instance.GetLocalizedText("network_error"), 
                    false, "", 
                    LocalizationManager.Instance.GetLocalizedText("retry"), 
                    null, () =>
                    {
                        waitRetry = false;
                    }
                );
                
                while (waitRetry) yield return null;
                yield return Patch();
            }
        }
    }
    
    private IEnumerator UpdateCatalog()
    {
        var initHandle = Addressables.InitializeAsync();
        yield return initHandle;
        
        var checkHandle = Addressables.CheckForCatalogUpdates(false);
        yield return checkHandle;

        //check if update is needed
        if (checkHandle.Status == AsyncOperationStatus.Succeeded)
        {
            if (checkHandle.Result.Count > 0)
            {
                //update handle
                var updateHandle = Addressables.UpdateCatalogs(checkHandle.Result, false);
                yield return updateHandle;
                Addressables.Release(updateHandle);
                
                LogUtil.Info("Update Catalog Completed", "yellow");
            }
            else
            {
                LogUtil.Info("No Catalog Update Detected", "yellow");
            }
        }

        Addressables.Release(checkHandle);
    }
    
    // check maintenance status with server, if maintenance should inform player and stop here
    private IEnumerator CheckServerMaintenanceAtStartUp()
    {
        yield break;
    }
    
    // check apk version with server, if too low prompt dialog direct player to update apk
    private IEnumerator CheckApkVersion()
    {
        yield break;
    }
    
    // set remote bundle load path, normally it's a cdn address
    private IEnumerator SetRemoteBundleLoadPath()
    {
        yield break;
    }
}
