using System;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// Singleton base for game object component.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MonoSingletonBase<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        private static bool _shuttingDown = false;

        public static T Instance
        {
            get
            {
                if (_shuttingDown)
                {
                    return null;
                }
                
                return _instance ? _instance : 
                    throw new InvalidOperationException($"Pre-instantiation missing. An instance of {typeof(T).Name} is required to exist in the scene.");
            }
        }

        protected virtual void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this as T;
            }
        }

        protected virtual void OnDestroy()
        {
            if (!_shuttingDown && _instance != null)
            {
                LogUtil.InfoFormat("Destroy MonoSingleton {0}", _instance.name);
            }

            if (_instance == this)
            {
                _instance = null;
            }
        }
        
        private void OnApplicationQuit()
        {
            _shuttingDown = true;
        }
    }
}
