﻿public static class LogUtil
{
    private enum LogLevel
    {
        Debug = 1,
        Info = 2,
        Warning = 3,
        Error = 4,
        None = 5,
    }

    private static LogLevel level = LogLevel.Warning;

    #region Default Log Function
    public static void Debug(string message, string color = "white")
    {
        if (level > LogLevel.Debug) return;
        UnityEngine.Debug.LogFormat("<color={0}>{1}</color>", color, message);
    }

    public static void DebugFormat(string message, params object[] parameters)
    {
        if (level > LogLevel.Debug) return;
        UnityEngine.Debug.LogFormat(message, parameters);
    }

    public static void Info(string message, string color = "white")
    {
        if (level > LogLevel.Info) return;
        UnityEngine.Debug.LogFormat("<color={0}>{1}</color>", color, message);
    }

    public static void InfoFormat(string message, params object[] parameters)
    {
        if (level > LogLevel.Info) return;
        UnityEngine.Debug.LogFormat(message, parameters);
    }

    public static void Warning(string message, string color = "white")
    {
        if (level > LogLevel.Warning) return;
        UnityEngine.Debug.LogWarningFormat("<color={0}>{1}</color>", color, message);
    }

    public static void WarningFormat(string message, params object[] parameters)
    {
        if (level > LogLevel.Warning) return;
        UnityEngine.Debug.LogWarningFormat(message, parameters);
    }

    public static void Error(string message, string color = "white")
    {
        if (level > LogLevel.Error) return;
        UnityEngine.Debug.LogErrorFormat("<color={0}>{1}</color>", color, message);
    }

    public static void ErrorFormat(string message, params object[] parameters)
    {
        if (level > LogLevel.Error) return;
        UnityEngine.Debug.LogErrorFormat(message, parameters);
    }
    #endregion
}
