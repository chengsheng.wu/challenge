using UnityEngine;

namespace Common
{
    public class GameUtils : SingletonBase<GameUtils>
    {
        public void ShowSystemInfo()
        {
            LogUtil.Debug("## System memory: " + SystemInfo.systemMemorySize + " MB");
            LogUtil.Debug("## Graphics API: " + SystemInfo.graphicsDeviceVersion);
        }
        
        public double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public long ConvertMegabytesToBytes(long megabytes)
        {
            return (megabytes * 1024) * 1024;
        }

        public SystemLanguage GetSystemOrPreferredLanguage()
        {
            if (PlayerPrefs.HasKey(Settings.LanguageKey))
            {
                return (SystemLanguage)PlayerPrefs.GetInt(Settings.LanguageKey);
            }

            return Application.systemLanguage;
        }

        public void SetPreferredLanguage(SystemLanguage lang)
        {
            PlayerPrefs.SetInt(Settings.LanguageKey,(int)lang);
        }

        public string GetBuildTarget()
        {
            #if UNITY_IOS
                return "iOS";
            #elif UNITY_ANDROID
                return "Android";
            #elif UNITY_STANDALONE_WIN
                return "StandaloneWindows";
            #elif UNITY_STANDALONE_OSX
                return "StandaloneOSX";
            #elif UNITY_WEBGL
                return "WebGL";
            #endif
        }

        public string FormatTime(int secondDiff, bool showHour)
        {
            if (secondDiff <= 0)
                return showHour ? "00:00:00" : "00:00";

            int days = Mathf.FloorToInt(secondDiff/86400);
            int hours = Mathf.FloorToInt((secondDiff%86400)/3600);
            int minutes = Mathf.FloorToInt((secondDiff/60)%60);
            int seconds = Mathf.FloorToInt(secondDiff%60);

            if (showHour)
            {
                if (days > 0)
                    return $"{days:00}:{hours:00}:{minutes:00}:{seconds:00}";
                return $"{hours:00}:{minutes:00}:{seconds:00}";
            }
            return $"{minutes:00}:{seconds:00}";
        }
    }
}