﻿public static class Settings
{
    public static string CDNAddress = "";
    public static string RemoteLoadPath = "";
    
    //User Preference
    public static string LanguageKey = "KEY_LANG";
    public static string MusicVolKey = "musicVolume";
    public static string EffectVolKey = "effectsVolume";
    public static string MasterVolKey = "masterVolume";
    public static string VibrationKey = "vibration";
    public static string FpsLimitKey = "fpsLimit";
    public static string DefaultLang = "en";

    //Addressable
    public static string DefaultLabel = "default";
    public static string BuiltInLabel = "built-in";
    public static string PrefabLabel = "prefab";
    public static string SpriteLabel = "sprite";
    public static string PreloadLabel = "preload";
    public static string PoolAssetLabel = "pool-asset";
    public static string LocalizationLabel = "localization";
    public static string FontLabel = "font";
    public static string TextLabel = "text";
    public static string SfxLabel = "sfx";
    public static string LuaLabel = "lua";
    public static string enLabel = "en";
    public static string cnLabel = "cn";

    //Other
    public static readonly float MasterVolume = 1f;
    public static readonly float MusicVolume = .3f;
    public static readonly float SfxVolume = .5f;
    public static readonly int FrameRateLow = 30;
    public static readonly int FrameRateHigh = 60;
}
