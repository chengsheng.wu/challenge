namespace Common
{
    /// <summary>
    /// This is the global coroutine helper to StartCoroutine for those statement not run inside MonoBehaviour
    /// We just need it to be MonoBehaviour so that we can handle coroutine
    /// </summary>
    public class CoroutineHelper : MonoSingletonBase<CoroutineHelper>
    { 
        //intentionally leave blank.
    }
}