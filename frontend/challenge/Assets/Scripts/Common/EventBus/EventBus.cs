using System;
using System.Collections.Generic;

namespace Common
{
    public struct EventBusListenerStatus
    {
        public readonly Action<object[]> listener;
        public readonly bool add; //true = Subscribe, false = UnSubscribe 

        public EventBusListenerStatus(Action<object[]> listener, bool add)
        {
            this.listener = listener;
            this.add = add;
        }
    }  
    
    public class EventBusListeners
    {
        private readonly List<Action<object[]>> listeners = new List<Action<object[]>>();
        private readonly List<EventBusListenerStatus> listenersToBeChanged = new List<EventBusListenerStatus>();
        private bool publishing;
        
        public void Subscribe(Action<object[]> listener)
        {
            //during publishing can't add/remove listeners which will throw exception.
            if (publishing)
            {
                listenersToBeChanged.Add(new EventBusListenerStatus(listener, true));
            }
            else
            {
                listeners.Add(listener);
            }
        }
        
        public void UnSubscribe(Action<object[]> listener)
        {
            //during publishing can't add/remove listeners which will throw exception.
            if (publishing)
            {
                listenersToBeChanged.Add(new EventBusListenerStatus(listener, false));
            }
            else
            {
                listeners.Remove(listener);
            }
        }

        public void Publish(object[] parameters)
        {
            if (listeners.Count == 0) return;
                      
            publishing = true;
            foreach (var listener in listeners)
            {
                listener.Invoke(parameters);
            }
            publishing = false;
            
            if (listenersToBeChanged.Count == 0) return;
            foreach (var listenStatue in listenersToBeChanged)
            {
                if (listenStatue.add)
                {
                    listeners.Add(listenStatue.listener);
                }
                else
                {
                    listeners.Remove(listenStatue.listener);
                }
            }
            listenersToBeChanged.Clear();
        }
    }
    
    public class EventBus : SingletonBase<EventBus>
    {   
        private readonly Dictionary<EventBusEventType, EventBusListeners> listenerGroups = new Dictionary<EventBusEventType, EventBusListeners>();

        public void Subscribe(EventBusEventType key, Action<object[]> listener)
        {
            if (!listenerGroups.TryGetValue(key, out var eventBusListeners))
            {
                eventBusListeners = new EventBusListeners();
                listenerGroups.Add(key, eventBusListeners);
            }        
            eventBusListeners.Subscribe(listener);
        }

        public void UnSubscribe(EventBusEventType key, Action<object[]> listener)
        {
            if (listenerGroups.TryGetValue(key, out var eventBusListeners))
            {
                eventBusListeners.UnSubscribe(listener);
            }        
        }

        public void Publish(EventBusEventType key, params object[] parameters)
        {
            if (listenerGroups.TryGetValue(key, out var eventBusListeners))
            {
                eventBusListeners.Publish(parameters);
            }                 
        }

        public void Clear()
        {
            listenerGroups.Clear();
        }
    }
}