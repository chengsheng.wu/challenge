using System;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    public static class MLFExtension
    {
        #region RectTransform Extension
        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }
        #endregion   
        
        public static void SetAlpha(this Image image, float alpha)
        {
            Color col = image.color;
            col.a = alpha;
            image.color = col;
        }
    }
}