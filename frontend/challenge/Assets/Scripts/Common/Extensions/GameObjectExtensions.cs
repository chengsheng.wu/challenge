﻿using Managers;
using UnityEngine;

namespace Common
{
    public static class GameObjectExtensions
    {
        public static bool IsNull(this UnityEngine.Object o)
        {
            return o == null;
        }

        public static void ForceRebuildLayout(this GameObject go)
        {
            var rect = go.GetComponent<RectTransform>();
            if (rect != null)
            {
                ForceRebuildLayout(rect);
            }
        }
        
        public static void ForceRebuildLayout(this RectTransform rect)
        {
            UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }
        
        public static void RemoveAllChildren(this GameObject parent)
        {
            RemoveAllChildren(parent.transform);
        }
    
        public static void RemoveAllChildren(this Transform parent)
        {
            Transform child;
            while (parent.childCount > 0)
            {
                child = parent.GetChild(0);
                //set parent to null first, otherwise in this frame parent still has this child
                child.SetParent(null);
                ResourceManager.Instance.ReleaseInstance(child.gameObject);
            }
        }

        public static void RemoveChild(this Transform parent, Transform child)
        {
            foreach (Transform t in parent)
            {
                if (t == child)
                {
                    t.SetParent(null);
                    ResourceManager.Instance.ReleaseInstance(child.gameObject);
                    return;
                }
            }
        }

        public static void RemoveChild(this Transform parent, string name)
        {
            Transform child;
            while (parent.childCount > 0)
            {
                child = parent.GetChild(0);
                if (child.name == name)
                {
                    //set parent to null first, otherwise in this frame parent still has this child
                    child.SetParent(null);
                    ResourceManager.Instance.ReleaseInstance(child.gameObject);
                }
            }
        }      
        
        public static void SetLayerRecursivelyByName(this Transform transform, string layerName)
        {
            var layer = LayerMask.NameToLayer(layerName);
            transform.SetLayerRecursivelyByLayer(layer);
        }
        
        public static void SetLayerRecursivelyByLayer(this Transform transform, int layer)
        {
            transform.gameObject.layer = layer;
            foreach (Transform child in transform)
            {
                child.SetLayerRecursivelyByLayer(layer);
            }
        }
    }
}