﻿using System;
using TMPro;
using UnityEngine;

public class SplashScreenGeneralPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageText;
    [SerializeField] private TextMeshProUGUI leftBtnText;
    [SerializeField] private TextMeshProUGUI rightBtnText;
    [SerializeField] private GameObject leftBtn;

    private Action leftBtnCB;
    private Action rightBtnCB;
    
    public void Show(string message, bool showLeftBtn, string leftBtnTxt, string rightBtnTxt, 
        Action leftBtnCB, Action rightBtnCB)
    {
        messageText.text = message;
        leftBtn.SetActive(showLeftBtn);
        this.leftBtnText.text = leftBtnTxt;
        this.rightBtnText.text = rightBtnTxt;
        this.leftBtnCB = leftBtnCB;
        this.rightBtnCB = rightBtnCB;
        gameObject.SetActive(true);
    }

    public void OnClick_LeftBtn()
    {
        this.leftBtnCB?.Invoke();
        gameObject.SetActive(false);
    }
    
    public void OnClick_RightBtn()
    {
        this.rightBtnCB?.Invoke();
        gameObject.SetActive(false);
    }   
}