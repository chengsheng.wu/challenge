using System;
using Common;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreen : MonoSingletonBase<SplashScreen>
{
    public SplashScreenGeneralPopup splashScreenGeneralPopup;
    public LanguageSetting languageSetting;
    public Image progressFill;
    public TextMeshProUGUI progressText;
    public TextMeshProUGUI versionText;

    public void SetVersion()
    {
        versionText.text = LocalizationManager.Instance.GetLocalizedText("version", Application.version);
    }

    public void SetProgress(string displayText, float fillAmount)
    {
        progressFill.fillAmount = fillAmount;
        progressText.text = displayText;
    }

    public void Notify(string message, bool showLeftBtn, string leftBtnTxt, string rightBtnTxt, 
        Action leftBtnCB, Action rightBtnCB)
    {
        splashScreenGeneralPopup.Show(message, showLeftBtn, leftBtnTxt, rightBtnTxt, leftBtnCB, rightBtnCB);
    }

    public string GetCurrentLangCode()
    {
        var currentLang = GameUtils.Instance.GetSystemOrPreferredLanguage();
        return languageSetting.GetCodeByLanguage(currentLang);
    }
}
