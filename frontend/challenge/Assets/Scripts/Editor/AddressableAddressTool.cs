﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sirenix.Utilities;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

public static class AddressableAddressTool
{
    private static string groupPrefix = "Auto_";
    
    private class AddressableConfig
    {
        public readonly string rootPath;
        public readonly bool includeRootPath;
        public readonly string groupName;
        public readonly bool recursive;
        public readonly List<string> labels;
        public readonly string pattern;

        public AddressableConfig(string rootPath, bool includeRootPath, string groupName, bool recursive, List<string> labels, string pattern)
        {
            this.rootPath = rootPath;
            this.includeRootPath = includeRootPath;
            this.groupName = groupName;
            this.recursive = recursive;
            this.labels = labels;
            this.pattern = pattern;
        }
    }

    private static List<AddressableConfig> GetAddressableConfigs()
    {
        var configList = new List<AddressableConfig>();

        // ui prefab
        configList.Add(new AddressableConfig(
            "Assets/Prefabs/UI/Windows", 
            true, 
            "UIPrefabs", 
            true,
            new List<string> {Settings.BuiltInLabel}, 
            "*.prefab")
        );

        return configList;
    }
    
    private static void MakeAddressableGroupByRootFolder(AddressableConfig config)
    {
        //for progress display
        int currentProgress = 0;
        int totalProgress = 0;
        
        //get target group name
        var settings = AddressableAssetSettingsDefaultObject.Settings;
        var targetGroup = GetGroupByName(groupPrefix + config.groupName);
        
        //get game object list
        var assetGuidList = new List<string>();
        var directories = Directory.GetDirectories(config.rootPath, config.pattern).ToList();
        if(config.includeRootPath) directories.Add(config.rootPath);
        totalProgress = directories.Count;
        currentProgress = 0;
        
        //get all file under direction
        foreach (var directory in directories)
        {
            if (config.recursive)
            {
                var files = Directory.GetFiles(directory, config.pattern, SearchOption.AllDirectories);
                assetGuidList.AddRange(files.Select(AssetDatabase.AssetPathToGUID));
            }
            else
            {
                assetGuidList.Add(AssetDatabase.AssetPathToGUID(directory));
            }

            currentProgress++;
            EditorUtility.DisplayProgressBar("Refreshing "+ config.groupName + " Addressable",
                directory, (float)currentProgress/ totalProgress);
        }
        
        //make prefab addressable using prefab gui list
        var entries = new List<AddressableAssetEntry>();
        foreach (var guid in assetGuidList)
        {
            var entry = settings.CreateOrMoveEntry(guid, targetGroup, false, false);
            config.labels.ForEach(s => entry.SetLabel(s, true));
            var assetPath = AssetDatabase.GUIDToAssetPath(guid);
            var spl = assetPath.Split('/');
            entry.address = config.groupName + "/" + spl[spl.Length - 1];
            entries.Add(entry);
        }
        
        //refresh
        settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entries, true);
        EditorUtility.ClearProgressBar();
    }

    private static void ClearGroupEntry(AddressableAssetGroup targetGroup)
    {
        var settings = AddressableAssetSettingsDefaultObject.Settings;
        var guidList = new List<string>();
        targetGroup.entries.ForEach(entry => guidList.Add(entry.guid));
        
        foreach (var guid in guidList)
        {
            settings.RemoveAssetEntry(guid);
            settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryRemoved, guid, true);
        }
    }

    private static AddressableAssetGroup GetGroupByName(string groupName)
    {
        foreach (var group in AddressableAssetSettingsDefaultObject.Settings.groups)
        {
            if (string.Equals(group.Name, groupName))
            {
                return group;
            }
        }
        
        throw new SystemException("Group Not Found :" + groupName);
    }

    [MenuItem("Tools/Addressable/Generate Addressable Group")]
    public static void AutoGenAddressableGroup()
    {
        ClearAddressableGroup();
        
        foreach (var config in GetAddressableConfigs())
        {
            MakeAddressableGroupByRootFolder(config);
        }
    }
    
    [MenuItem("Tools/Addressable/Clear Addressable Group")]
    public static void ClearAddressableGroup()
    {
        foreach (var config in GetAddressableConfigs())
        {
            var targetGroup = GetGroupByName(groupPrefix + config.groupName);
            ClearGroupEntry(targetGroup);
        }
    }
}
