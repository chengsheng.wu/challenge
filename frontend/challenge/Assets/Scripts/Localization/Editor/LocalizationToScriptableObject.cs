﻿using System.Collections.Generic;
using System.IO;
using Localization;
using UnityEditor;
using Debug = UnityEngine.Debug;
using CsvHelper;
using UnityEngine;

public class LocalizationToScriptableObject
{
    private static string ROOT_PATH = "Assets/Localizations/";
    private static readonly string PATH_LOCALIZATION_CSV = ROOT_PATH + "Csv";
    private static readonly string PATH_LOCALIZATION_DATA = ROOT_PATH + "Data";
    private static readonly string PATH_SPLASH_LOCALIZATION_DATA = ROOT_PATH + "Resources";

    [MenuItem("Tools/Localization/Build Localization Container Data")]
    public static void BuildLocalizationScriptableData()
    {
        if (EditorUtility.DisplayDialog(
            "Data Overwrite Warning",
            "Are you sure you want to continue ? This will delete all existing created container data and recreate",
            "Yes", "No"))
        {
            var folders = GetAllCsvFolders(); // all language csv folder
            var totalProgressCount = folders.Length; // for progress bar display
            var currentProgressCount = 1; // for progress bar display

            foreach (var folder in folders)
            {
                //get current language name by folder name
                string langName = folder.Split('\\')[1];

                //get save path under data folder
                string savePath = GetScriptableSavePath(langName);
                string splash_savePath = GetSplashScriptableSavePath(langName);

                DeleteDataInFolder(savePath);
                DeleteDataInFolder(splash_savePath);

                //create directory 
                Directory.CreateDirectory(savePath);
                Directory.CreateDirectory(splash_savePath);

                //get all csv path in the folder
                string[] files = GetAllCsvFilesAtPath(folder);

                //variable for progress bar display
                int totalFilesCount = files.Length;
                int currentFileCount = 0;
            
                foreach (var file in files)
                {
                    //load csv as container
                    var container = ScriptableObject.CreateInstance<LocalizationContainer>();
                    
                    ParseCsv(file, container);

                    //get container name
                    string[] spl = file.Split('.')[0].Split('\\');
                    container.name = spl[spl.Length - 1];

                    //create asset at path
                    CreateContainerAssetsAtPath(container.name.StartsWith("splash_") ? splash_savePath : savePath, container);

                    //update progress bar
                    currentFileCount++;
                    string progressStr = "{0}/{1} [{2}] {3}";
                    EditorUtility.DisplayProgressBar(
                        "Building Scriptable Object",
                        string.Format(progressStr, currentProgressCount, totalProgressCount, langName, container.name),
                        (float)currentFileCount/ totalFilesCount);
                }                            
                //update progress bar
                currentProgressCount++;       
            }

            //save created assets
            AssetDatabase.SaveAssets();
      
            //post creating handling
            EditorUtility.ClearProgressBar();                
            AssetDatabase.Refresh();
        }
    }

    public static bool ValidateCsv()
    {
        var folders = GetAllCsvFolders(); // all language csv folder
        Dictionary<string, string> splashLocalizationMap = new Dictionary<string, string>();
        Dictionary<string, string> inGameLocalizationMap = new Dictionary<string, string>();
        
        foreach (var folder in folders)
        {
            //get current language name by folder name
            string langName = folder.Split('\\')[1];
            //get all csv path in the folder
            string[] files = GetAllCsvFilesAtPath(folder);

            splashLocalizationMap.Clear();
            inGameLocalizationMap.Clear();
            
            foreach (var file in files)
            {
                //load csv as container
                var container = ScriptableObject.CreateInstance<LocalizationContainer>();
                ParseCsv(file, container);

                //get container name
                string[] spl = file.Split('.')[0].Split('\\');
                container.name = spl[spl.Length - 1];

                var map = container.name.StartsWith("splash_") ? splashLocalizationMap : inGameLocalizationMap;
                foreach (var pair in container.data)
                {
                    if (map.ContainsKey(pair.key))
                    {
                        Debug.LogErrorFormat("Lang:{0} TextKey:{1} duplicated in file {2} and {3}", langName, pair.key, container.name, map[pair.key]);
                        return false;
                    }
                    else
                    {
                        map[pair.key] = container.name;
                    }
                }
                GameObject.DestroyImmediate(container);
            }
        }

        return true;
    }

    static void DeleteDataInFolder(string folder)
    {
        if (Directory.Exists(folder))
        {
            //delete all files under directory
            string[] currentFiles = Directory.GetFiles(folder, "*.asset");
            foreach (var aFile in currentFiles)
            {
                Debug.Log("delete file:" + aFile);
                File.Delete(aFile);
            }
        }
    }
    
    static string[] GetAllCsvFolders()
    {
        return Directory.GetDirectories(PATH_LOCALIZATION_CSV);
    }

    static string GetScriptableSavePath(string langName)
    {
        return PATH_LOCALIZATION_DATA + "/" + langName + "/";
    }
    
    static string GetSplashScriptableSavePath(string langName)
    {
        return PATH_SPLASH_LOCALIZATION_DATA + "/" + langName + "/";
    }   

    static string[] GetAllCsvFilesAtPath(string path)
    {
        return Directory.GetFiles(path, "*.csv");
    }

    static void CreateContainerAssetsAtPath(string savePath, LocalizationContainer toSave)
    {
        AssetDatabase.CreateAsset(toSave, savePath + toSave.name + ".asset");         
    }
    
    static void ParseCsv(string path, LocalizationContainer container)
    {
        // read in the csv
        StreamReader streamReader = new StreamReader(path, System.Text.Encoding.UTF8);
        string csvText = streamReader.ReadToEnd();
        streamReader.Close();
        
        using (StringReader csvReader = new StringReader(csvText)) 
        {
            using (var reader = new CsvReader(csvReader))
            {
                var records = reader.GetRecords(typeof(Localization.KeyPairValue));
                container.Process(records);
            }
        }
    }
}