﻿using Managers;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Localization
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedText : MonoBehaviour
    {
        public bool staticText;
#if UNITY_EDITOR
        [OnValueChanged(nameof(HandleStaticTextChanged)), ShowIf(nameof(staticText))]
#endif
        public string textId;
     
        private TextMeshProUGUI targetText;
        private object[] parameters;
        
        private void Awake()
        {
            LazyInit();
        }

        private void Start()
        {
            if (staticText) 
                RefreshTextByTextId();
        }

        private void LazyInit()
        {
            if (targetText == null)
                targetText = GetComponent<TextMeshProUGUI>();
        }
        
        private void RefreshTextByTextId()
        {
            LazyInit();
            
            if (string.IsNullOrEmpty(textId) || targetText == null)
                return;

            var newText = LocalizationManager.Instance.GetLocalizedText(textId);
            targetText.text = parameters != null ? string.Format(newText, parameters) : newText;
        }

        public void SetTextId(string aTextId, params object[] param)
        {
            parameters = param;
            textId = aTextId;
            RefreshTextByTextId();
        }

        public void SetPlainText(string plainText)
        {
            LazyInit();
            
            if (targetText == null)
                return;
            
            targetText.text = plainText;
        }
        
#if UNITY_EDITOR
        [OnInspectorGUI]
        private void OnInspectorGUI()
        {
            LazyInit();
        }

        private void HandleStaticTextChanged()
        {
            if (targetText == null) 
                return;
            targetText.text = "#" + textId;
        }
#endif
    }
}
