﻿using System.Collections;
using System.Collections.Generic;
using Common;
using Localization;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Managers
{
    public class LocalizationManager : SingletonBase<LocalizationManager>
    {
        private readonly Dictionary<string, string> localizationData = new Dictionary<string, string>();
        public LanguageSetting LanguageSetting { get; private set; }

        public void SplashScreenPreload()
        {
            LanguageSetting = Resources.Load<LanguageSetting>("LanguageSetting");
            
            var langCode = GetCurrentLangCode();

            // load localization container by language
            localizationData.Clear();
            var container = Resources.Load<LocalizationContainer>($"{langCode}/splash_common");
            foreach (var pair in container.data)
                localizationData.Add(pair.key, pair.value);
        }
        
        public IEnumerator Preload()
        {
            var settingHandle = Addressables.LoadAssetAsync<LanguageSetting>("LanguageSetting.asset");
            yield return settingHandle;
            LanguageSetting = settingHandle.Result;
            Addressables.Release(settingHandle);
            
            var langCode = GetCurrentLangCode();

            // load localization container by language
            var handle = Addressables.LoadAssetsAsync<LocalizationContainer>((IEnumerable)new List<object> {langCode, "text"},
                null, Addressables.MergeMode.Intersection);
            yield return handle;
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                // process language data
                localizationData.Clear();
                foreach (var container in handle.Result)
                    foreach (var pair in container.data)
                        localizationData.Add(pair.key, pair.value);
            }
            Addressables.Release(handle);
        }
        
        public void Cleanup()
        {
            localizationData.Clear();
        }
        
        public string GetLocalizedText(string textId)
        {
            if (string.IsNullOrEmpty(textId)) return "not found:";
            if (localizationData.TryGetValue(textId, out var value)) return value;
            return "#" + textId;
        }

        public string GetLocalizedText(string textId, params object[] parameters)
        {
            var newText = GetLocalizedText(textId);
            return parameters != null ? string.Format(newText, parameters) : newText;
        }

        public string GetCurrentLangCode()
        {
            var currentLang = GameUtils.Instance.GetSystemOrPreferredLanguage();
            return LanguageSetting.GetCodeByLanguage(currentLang);
        }
    }
}