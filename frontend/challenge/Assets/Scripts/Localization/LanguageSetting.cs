﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSetting : ScriptableObject
{
    public string defaultCode = "en";
    public SystemLanguage defaultLanguage = SystemLanguage.English;
    public List<LanguageMap> supportedLanguages;

    public string GetCodeByLanguage(SystemLanguage language)
    {
        foreach (var map in supportedLanguages)
            if (language == map.systemLanguage)
                return map.languageCode;

        return defaultCode;
    }

    public SystemLanguage GetLanguageByCode(string code)
    {
        foreach (var map in supportedLanguages)
            if (code == map.languageCode)
                return map.systemLanguage;

        return defaultLanguage;
    }
}

[Serializable]
public class LanguageMap
{
    public string languageCode;
    public SystemLanguage systemLanguage;
}