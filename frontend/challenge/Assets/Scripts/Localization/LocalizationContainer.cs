﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Localization
{
    [Serializable]
    public class LocalizationContainer : ScriptableObject
    {
        [ListDrawerSettings(HideAddButton = true, HideRemoveButton = true, ShowPaging = false)]
        public List<KeyPairValue> data;

        public void Process(IEnumerable<object> rawData)
        {
            data = rawData.OfType<KeyPairValue>().ToList();
        }
    }

    [Serializable]
    public class KeyPairValue
    {
        [HorizontalGroup] [HideLabel] public string key;

        [HorizontalGroup] [HideLabel] public string value;

        public KeyPairValue(string key, string value)
        {
            this.key = key;
            this.value = value;
        }
    }
}