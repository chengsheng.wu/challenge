﻿using Common;
using UI.Components;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class DeckAndHeroWidget : MonoBehaviour
    {
        [SerializeField] private CompDeck compDeck;
        [SerializeField] private Transform heroParent;
        [SerializeField] private GameObject cardPrefab;

        public void SetData(DeckData deckData)
        {
            compDeck.SetData(deckData);
            heroParent.RemoveAllChildren();

            var uniqueHeroes = deckData.GetUniqueHeroCardIds();
            foreach (var heroCardId in uniqueHeroes)
                SpawnCard(heroCardId);
        }
        
        private void SpawnCard(int cardId)
        {
            var cardObj = Instantiate(cardPrefab, heroParent, false);
            var compCard = cardObj.GetComponent<CompCard>();
            compCard.SetData(cardId);
        }
    }
}