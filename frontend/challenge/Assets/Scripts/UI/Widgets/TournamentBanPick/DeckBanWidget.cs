﻿using UI.Windows.Common;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class DeckBanWidget : MonoBehaviour
    {
        [SerializeField] private DeckAndHeroWidget deckAndHeroWidget;

        private TournamentBanPickWindow parentWindow;
        private DeckData deckData;
        
        public void SetData(DeckData deckData, TournamentBanPickWindow parent)
        {
            parentWindow = parent;
            this.deckData = deckData;
            deckAndHeroWidget.SetData(deckData);
        }

        public void OnClickedBan()
        {
            parentWindow.Ban(deckData.ID, false);
        }
    }
}