﻿using PlayerData.Tournament;
using UI.Components;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class PlayerVsWidget : MonoBehaviour
    {
        [SerializeField] private CompPlayerProfile compOpponentProfile;
        [SerializeField] private CompPlayerProfile compPlayerProfile;

        public void SetData(TournamentPlayerData player, TournamentPlayerData opponent)
        {
            var playerData = player.GetPlayerData();
            compPlayerProfile.SetData(playerData.profileIconId, playerData.name, player.Status);
            
            var opponentData = opponent.GetPlayerData();
            compOpponentProfile.SetData(opponentData.profileIconId, opponentData.name, opponent.Status);
        }

        public CompPlayerProfile GetPlayerProfile()
        {
            return compPlayerProfile;
        }
        
        public CompPlayerProfile GetOpponentProfile()
        {
            return compOpponentProfile;
        }        
    }
}