﻿using System.Collections.Generic;
using Common;
using Managers;
using UI.Components;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class DeckCardListWidget : MonoBehaviour
    {
        [SerializeField] private CompRegions compRegions;
        [SerializeField] private Transform cardParent;
        [SerializeField] private GameObject cardPrefab;

        public void SetData(DeckData deckData)
        {
            cardParent.RemoveAllChildren();
            compRegions.SetRegion(deckData);
            var cardIdAmount = new Dictionary<int, int>(); 
            foreach (var cardId in deckData.CardIds)
            {
                if (cardIdAmount.TryGetValue(cardId, out var amount))
                    cardIdAmount[cardId] = amount + 1;
                else
                    cardIdAmount[cardId] = 1;
            }

            foreach (var kvp in cardIdAmount)
                SpawnCard(kvp.Key, kvp.Value);
        }

        private void SpawnCard(int cardId, int amount)
        {
            var cardObj = Instantiate(cardPrefab, cardParent, false);
            cardObj.GetComponent<CompCardInList>().SetData(cardId, amount);
        }
    }
}