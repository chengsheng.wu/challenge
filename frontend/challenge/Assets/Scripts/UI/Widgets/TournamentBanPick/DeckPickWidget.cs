﻿using UI.Windows.Common;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class DeckPickWidget : MonoBehaviour
    {
        [SerializeField] private DeckAndHeroWidget deckAndHeroWidget;

        private TournamentBanPickWindow parentWindow;
        private DeckData deckData;
        
        public void SetData(DeckData deckData, TournamentBanPickWindow parent)
        {
            parentWindow = parent;
            this.deckData = deckData;
            deckAndHeroWidget.SetData(deckData);
        }

        public void OnClickedChoose()
        {
            parentWindow.Pick(deckData.ID, true);
        }
    }
}