﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using Managers;
using PlayerData.Tournament;
using TMPro;
using UI.Components;
using UI.Windows.Common;
using UnityEngine;

namespace UI.Widgets.TournamentBanPick
{
    public class DeckVsWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private RectTransform midRectTransform;
        [SerializeField] private Transform opponentDeckParent;
        [SerializeField] private Transform playerDeckParent;
        [SerializeField] private GameObject deckPrefab;
        [SerializeField] private RectTransform countdownFillRectTransform;
        [SerializeField] private TextMeshProUGUI countdownText;
        
        private TournamentBanPickWindow parentWindow;
        private Dictionary<int, CompDeck> playerIdDeckMap = new Dictionary<int, CompDeck>();
        private Dictionary<int, CompDeck> opponentIdDeckMap = new Dictionary<int, CompDeck>();

        public void SetData(TournamentPlayerData player, TournamentPlayerData opponent, TournamentBanPickWindow parent)
        {
            parentWindow = parent;

            opponentDeckParent.RemoveAllChildren();
            playerDeckParent.RemoveAllChildren();
            playerIdDeckMap.Clear();
            opponentIdDeckMap.Clear();
            
            midRectTransform.SetRight(0);
            
            foreach (var deckData in opponent.GetDecks())
                SpawnDeck(deckData, false);
            
            foreach (var deckData in player.GetDecks())
                SpawnDeck(deckData, true);
        }

        public void UpdateStage(MatchMakingStage matchMakingStage)
        {
            StopAllCoroutines();
            switch (matchMakingStage)
            {
                case MatchMakingStage.Prepare:
                case MatchMakingStage.Ban: 
                case MatchMakingStage.Pick:
                    titleText.text = LocalizationManager.Instance.GetLocalizedText($"tournament_ban_pick_title_{matchMakingStage}");
                    StartCoroutine(UpdateCountdown(GameDataManager.Instance.GetConstantInt(
                        $"Seasonal_Tournament_Countdown_{matchMakingStage}")));
                    break;
                
                case MatchMakingStage.MatchStarting:
                    titleText.text = LocalizationManager.Instance.GetLocalizedText($"tournament_ban_pick_title_{matchMakingStage}");
                    break;
            }
        }

        public void SetRectTransformRightOffset(float offset)
        {
            midRectTransform.SetRight(offset);
        }

        public void OnDeckBanned(int deckId, bool isPlayerDeck)
        {
            var idDeckMap = GetIdDeckMap(isPlayerDeck);
            if (idDeckMap.TryGetValue(deckId, out var compDeck))
                compDeck.UpdateCross(true);
        }
        
        public void OnDeckPicked(int deckId, bool isPlayerDeck)
        {
            var idDeckMap = GetIdDeckMap(isPlayerDeck);
            if (idDeckMap.TryGetValue(deckId, out var compDeck))
                compDeck.UpdateSelection(true);
        }

        private Dictionary<int, CompDeck> GetIdDeckMap(bool isPlayerDeck)
        {
            return isPlayerDeck ? playerIdDeckMap : opponentIdDeckMap;
        }

        private void SpawnDeck(DeckData deckData, bool isPlayerDeck)
        {
            var deckObj = Instantiate(deckPrefab, isPlayerDeck ? playerDeckParent : opponentDeckParent, false);
            var compDeck = deckObj.GetComponent<CompDeck>();
            compDeck.SetData(deckData);
            compDeck.GetButton().onClick.AddListener(() => OnClickCB(compDeck, isPlayerDeck));
            GetIdDeckMap(isPlayerDeck)[deckData.ID] = compDeck;
        }
        
        private void OnClickCB(CompDeck compDeck, bool isPlayerDeck)
        {
            parentWindow.OnClickCB(compDeck, isPlayerDeck);
        }

        private IEnumerator UpdateCountdown(int inspectCountdown)
        {
            float total = inspectCountdown;
            float timeLeft = inspectCountdown;

            while (timeLeft > 0)
            {            
                countdownText.text = GameUtils.Instance.FormatTime(Mathf.CeilToInt(timeLeft), false);
                float halfWidth = (total - timeLeft) / total * midRectTransform.rect.width * 0.5f;
                countdownFillRectTransform.SetLeft(halfWidth);
                countdownFillRectTransform.SetRight(halfWidth);
                yield return null;
                timeLeft -= Time.deltaTime;
            }
            
            countdownText.text = GameUtils.Instance.FormatTime(0, false);
            parentWindow.OnStageTimeout();
        }
    }
}