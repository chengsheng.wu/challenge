﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using GameData;
using Managers;
using TMPro;
using UI.Components;
using UI.Windows.Common;
using UnityEngine;

namespace UI.Widgets.SelectDeck
{
    public class DeckSelectionWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TMP_Dropdown dropdownFilter;
        [SerializeField] private TMP_Dropdown dropdownSort;
        [SerializeField] private Transform deckParent;
        [SerializeField] private GameObject deckPrefab;
        [SerializeField] private GameObject noDeckContainer;
        
        private SelectDeckWindow parentWindow;
        private readonly List<CompDeck> spawnedDecks = new List<CompDeck>();
        private List<CompDeck> selectedDecks = new List<CompDeck>();
        private int Seasonal_Tournament_Deck_Max = 3; //will be overwrite in SetData with value get from constant_info

        private void Awake()
        {
            var localizationManager = LocalizationManager.Instance;
            dropdownFilter.options.Add(new TMP_Dropdown.OptionData(localizationManager.GetLocalizedText("all_regions")));
            var regions = Enum.GetValues(typeof(CardRegion));
            foreach (CardRegion region in regions)
            {
                if (region == CardRegion.Unknown)
                    continue;
                dropdownFilter.options.Add(new TMP_Dropdown.OptionData(localizationManager.GetLocalizedText(string.Format("region_{0}", region))));
            }
            
            dropdownSort.options.Add(new TMP_Dropdown.OptionData("A-Z"));
            dropdownSort.options.Add(new TMP_Dropdown.OptionData("Z-A"));
        }
        
        public void SetData(SelectDeckWindow selectDeckWindow)
        {
            parentWindow = selectDeckWindow;
            Seasonal_Tournament_Deck_Max = GameDataManager.Instance.GetConstantInt("Seasonal_Tournament_Deck_Max", 3);
            
            deckParent.RemoveAllChildren();
            spawnedDecks.Clear();
            selectedDecks.Clear();
            dropdownFilter.value = 0;
            dropdownSort.value = 0;

            titleText.text =
                LocalizationManager.Instance.GetLocalizedText("select_deck_window_title", Seasonal_Tournament_Deck_Max);
            
            var decks = PlayerDeckManager.Instance.GetPlayerDecks();
            foreach (var kvp in decks)
            {
                //assume deck with 0 card won't be allowed bring to battle, so don't spawn it
                if (kvp.Value.IsEmpty())
                    continue;
                SpawnDeck(kvp.Value);
            }
            OnFilterChanged(dropdownFilter.value);
            OnSortChanged(dropdownSort.value);
        }

        public void ResetSelection()
        {
            foreach (var compDeck in selectedDecks)
                compDeck.UpdateSelection(false);
            
            selectedDecks.Clear();
            dropdownFilter.value = 0;
            dropdownSort.value = 0;
        }

        private void SpawnDeck(DeckData deckData)
        {
            var deckObj = Instantiate(deckPrefab, deckParent, false);
            var compDeck = deckObj.GetComponent<CompDeck>();
            compDeck.SetData(deckData);
            compDeck.GetButton().onClick.AddListener(() => OnClickCB(compDeck));
            spawnedDecks.Add(compDeck);
        }

        private void OnClickCB(CompDeck compDeck)
        {
            int index = selectedDecks.IndexOf(compDeck);
            // if it's already selected, then unselect it
            if (index >= 0)
            {
                compDeck.UpdateSelection(false);
                selectedDecks.RemoveAt(index);
            } 
            else if (selectedDecks.Count < Seasonal_Tournament_Deck_Max)
            {
                compDeck.UpdateSelection(true);
                selectedDecks.Add(compDeck);
                if (selectedDecks.Count >= Seasonal_Tournament_Deck_Max)
                    parentWindow.OnSelectionFinished(selectedDecks.Select(x => x.GetDeckData()).ToList());
            }
        }

        public void OnFilterChanged(int filterIndex)
        {
            //index 0 means show all region
            if (filterIndex == 0)
            {
                foreach (var compDeck in spawnedDecks)
                    compDeck.gameObject.SetActive(true);
                noDeckContainer.SetActive(spawnedDecks.Count == 0);
                return;
            }

            //since index 0 is "All Region" and real region start from index 1
            var regionDiff = filterIndex - 1;
            var cardRegion = regionDiff + CardRegion.Unknown + 1;
            bool hasValidDeck = false;
            foreach (var compDeck in spawnedDecks)
            {
                if (compDeck.GetDeckData().HasRegion(cardRegion))
                {
                    hasValidDeck = true;
                    compDeck.gameObject.SetActive(true);
                }
                else
                    compDeck.gameObject.SetActive(false);
            }
            
            noDeckContainer.SetActive(!hasValidDeck);
        }

        public void OnSortChanged(int sortIndex)
        {
            //sort spawned deck by deck name ascending
            if (sortIndex == 0)
                spawnedDecks.Sort((x, y) => String.Compare(x.GetDeckData().Name, y.GetDeckData().Name, StringComparison.Ordinal));
            else 
                spawnedDecks.Sort((x, y) => String.Compare(y.GetDeckData().Name, x.GetDeckData().Name, StringComparison.Ordinal));
            
            for (int index = 0; index < spawnedDecks.Count; index++)
                spawnedDecks[index].transform.SetSiblingIndex(index);
        }
    }
}