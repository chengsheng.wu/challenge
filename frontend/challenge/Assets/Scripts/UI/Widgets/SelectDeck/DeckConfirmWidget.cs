﻿using System.Collections.Generic;
using Common;
using DG.Tweening;
using UI.Components;
using UI.Windows.Common;
using UnityEngine;

namespace UI.Widgets.SelectDeck
{
    public class DeckConfirmWidget : MonoBehaviour
    {
        [SerializeField] private Transform deckParent;
        [SerializeField] private GameObject deckPrefab;
        
        private SelectDeckWindow parentWindow;
        
        public void SetData(SelectDeckWindow selectDeckWindow, List<DeckData> selectedDecks)
        {
            parentWindow = selectDeckWindow;
            
            deckParent.RemoveAllChildren();

            int index = 0;
            foreach (var deckData in selectedDecks)
            {
                SpawnDeck(deckData, index);
                index++;
            }
        }

        private void SpawnDeck(DeckData deckData, int index)
        {
            var deckObj = Instantiate(deckPrefab, deckParent, false);
            var compDeck = deckObj.GetComponent<CompDeck>();
            compDeck.SetData(deckData);
            compDeck.UpdateSelection(true);
            deckObj.transform.localScale = Vector3.zero;
            deckObj.transform.DOScale(Vector3.one, 0.25f).SetDelay(index * 0.25f);
        }

        public void OnClickConfirm()
        {
            parentWindow.OnConfirmSelection();
        }

        public void OnClickedBack()
        {
            parentWindow.OnBackSelection();
        }
    }
}