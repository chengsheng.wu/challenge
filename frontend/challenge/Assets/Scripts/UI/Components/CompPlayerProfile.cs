﻿using Common;
using Managers;
using PlayerData.Tournament;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Components
{
    public class CompPlayerProfile : MonoBehaviour
    {
        [SerializeField] private Image profileImageOutline;
        [SerializeField] private Image profileIconImage;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI statusText;
        [SerializeField] private Color readyColor;
        [SerializeField] private CompRegions compRegions;

        public void SetData(int profileIconId, string playerName, MatchMakingStatus status)
        {
            profileIconImage.sprite = ResourceManager.Instance.LoadSprite("PlayerIcons", profileIconId.ToString());
            nameText.text = playerName;
            UpdateStatus(status);
            compRegions.gameObject.SetActive(false);
        }

        public void UpdateStatus(MatchMakingStatus status)
        {
            statusText.text = LocalizationManager.Instance.GetLocalizedText(string.Format("match_making_status_{0}", status));
            statusText.color = status == MatchMakingStatus.Ready ? readyColor : Color.white;
            profileImageOutline.color = status == MatchMakingStatus.Ready ? readyColor : Color.black;
        }

        public void SetRegion(DeckData deckData)
        {
            compRegions.SetRegion(deckData);
            compRegions.gameObject.SetActive(true);
        }
    }
}