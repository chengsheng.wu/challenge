﻿using Common;
using UnityEngine;

namespace UI.Components
{
    public class CompRegions : MonoBehaviour
    {
        [SerializeField] private Transform regionParent;
        [SerializeField] private GameObject regionPrefab;

        public void SetRegion(DeckData deckData)
        {
            CleanUp();
            var regions = deckData.GetRegions();
            foreach (var kvp in regions)
            {
                var regionObj = Instantiate(regionPrefab, regionParent, false);
                regionObj.GetComponent<CompRegion>().SetData(kvp.Key);
            }
        }

        public void CleanUp()
        {
            regionParent.RemoveAllChildren();
        }
    }
}