﻿using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Components
{
    public class CompCardInList : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image cardBg;
        [SerializeField] private TextMeshProUGUI cardNameText;
        [SerializeField] private TextMeshProUGUI energyText;
        [SerializeField] private TextMeshProUGUI cardAmountText;
        [SerializeField] private GameObject heroTag;

        public void SetData(int cardId, int amount)
        {
            //due to lack of assets, i will not update cardBg, cardBg rarity base on card info.
            //but they are easy to integrate once got assets
            var cardInfo = GameDataManager.Instance.GetCardInfoById(cardId);
            cardNameText.text = LocalizationManager.Instance.GetLocalizedText(cardInfo.name);
            energyText.text = cardInfo.energe_cost.ToString();
            cardAmountText.text = amount.ToString();
            heroTag.SetActive(cardInfo.hero);
        }
    }
}