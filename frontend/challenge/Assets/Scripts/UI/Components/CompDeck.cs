﻿using System;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Components
{
    public class CompDeck : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image bgImage;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private CompRegions compRegions;
        [SerializeField] private GameObject selectedObj;
        [SerializeField] private GameObject tickObj;
        [SerializeField] private GameObject crossObj;
        [SerializeField] private GameObject blockerObj;
        [SerializeField] private Material greyMaterial;

        private DeckData deckData;
        private bool selected = false;

        public void SetData(DeckData deckData)
        {
            this.deckData = deckData;
            bgImage.sprite = ResourceManager.Instance.LoadSprite(deckData.IconName);
            nameText.text = deckData.Name;
            compRegions.SetRegion(deckData);
            
            UpdateSelection(false);
            UpdateCross(false);
            UpdateBlocker(false);
        }

        public void UpdateSelection(bool selected)
        {
            this.selected = selected;
            selectedObj.SetActive(selected);
            tickObj.SetActive(selected);
        }

        public void UpdateCross(bool show)
        {
            crossObj.SetActive(show);
            bgImage.material = show ? greyMaterial : null;
            button.interactable = !show;
        }

        public void UpdateBlocker(bool show)
        {
            blockerObj.SetActive(show);
        }

        public DeckData GetDeckData()
        {
            return deckData;
        }

        public Button GetButton()
        {
            return button;
        }
    }
}