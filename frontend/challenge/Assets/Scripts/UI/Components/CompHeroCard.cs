﻿using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Components
{
    public class CompHeroCard : MonoBehaviour
    {
        [SerializeField] private Image cardBg;
        [SerializeField] private TextMeshProUGUI cardNameText;

        public void SetData(int cardId)
        {
            var cardInfo = GameDataManager.Instance.GetCardInfoById(cardId);
            cardBg.sprite = ResourceManager.Instance.LoadSprite(cardInfo.icon);
            cardNameText.text = LocalizationManager.Instance.GetLocalizedText(cardInfo.name);
        }
    }
}