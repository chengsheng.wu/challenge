﻿using GameData;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Components
{
    public class CompRegion : MonoBehaviour
    {
        [SerializeField] private Image regionImage;

        public void SetData(CardRegion region)
        {
            regionImage.sprite = ResourceManager.Instance.LoadSprite("Common", string.Format("region_{0}", region));
        }
    }
}