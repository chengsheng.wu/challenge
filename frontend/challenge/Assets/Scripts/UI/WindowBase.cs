﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public abstract class WindowBase : MonoBehaviour
    {
        public WindowType windowType = WindowType.None;
        public WindowLayer windowLayer = WindowLayer.Normal;
        public WindowShowMode windowShowMode = WindowShowMode.Normal;
        public Button closeButton;

        protected virtual void Awake()
        {
            if (windowType == WindowType.None)
                return;
            
            if (closeButton != null)
            {
                closeButton.onClick.AddListener(CloseWindow);
            }
        }

        protected virtual void CloseWindow()
        {
            UIManager.Instance.CloseWindow(windowType);
        }

        public bool IsOpen()
        {
            return gameObject.activeInHierarchy;
        }

        // in case we want to do something in future, easy for expansion
        public virtual void Display()
        {
            gameObject.SetActive(true);
        }

        // in case we want to do something in future, easy for expansion
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}