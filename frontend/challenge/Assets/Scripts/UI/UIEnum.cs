﻿namespace UI
{
    //make sure WindowType name match window prefab name so that we can load correct prefab from Addressables
    public enum WindowType
    {
        // hardcode value to prevent someone insert type between other types which will make windowType wrong in WindowBase 
        // for existing window prefabs because windowType is serialized in window prefab
        // and make sure WindowType is same as window prefab name
        None = 0,
        GeneralPopup = 1,
        LoadingWindow = 2,
        SelectDeckWindow = 3,
        TournamentBanPickWindow = 4,
    }

    public enum WindowLayer
    {
        // hardcode value to prevent someone insert layer between other layers which will make windowLayer wrong in WindowBase
        // for existing window prefabs because windowLayer is serialized in window prefab
        Normal = 1,
        Hud = 2,
        Popup = 3,
        Notification = 4,
    }

    public enum WindowShowMode
    {
        // hardcode value to prevent someone insert show mode between other modes which will make windowShowMode wrong in WindowBase
        // for existing window prefabs because windowShowMode is serialized in window prefab
        Normal = 1,
        Popup = 2,
        Notification = 3,
    }
}