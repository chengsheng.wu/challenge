﻿using Managers;
using UnityEngine.UI;

namespace UI.Windows.Common
{
    public class LoadingWindow : WindowBase
    {
        public Image progressFillImg;
        
        public override void Display()
        {
            base.Display();
            ClearProgress();
        }

        private void Update()
        {
            float currentProgress = SceneManager.Instance.Progress;
            progressFillImg.fillAmount = currentProgress;
        }

        private void ClearProgress()
        {
            progressFillImg.fillAmount = 0;
        }
    }
}