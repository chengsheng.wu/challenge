﻿using System.Collections.Generic;
using Network;
using PlayerData.Tournament;
using UI.Components;
using UI.Widgets.TournamentBanPick;
using UnityEngine;

namespace UI.Windows.Common
{
    public class TournamentBanPickWindow : WindowBase
    {
        [SerializeField] private PlayerVsWidget playerVsWidget;
        [SerializeField] private DeckCardListWidget deckCardListWidget;
        [SerializeField] private DeckVsWidget deckVsWidget;
        [SerializeField] private DeckBanWidget deckBanWidget;
        [SerializeField] private DeckPickWidget deckPickWidget;

        private MatchMakingStage matchMakingStage = MatchMakingStage.Prepare;
        private TournamentPlayerData playerData;
        private TournamentPlayerData opponentData;
        private float banWidgetWidth = 420;
        private int playerBannedDeckId;
        private int playerPickedDeckId;
        private int opponentBannedDeckId;
        private int opponentPickedDeckId;

        protected override void Awake()
        {
            base.Awake();
            banWidgetWidth = deckBanWidget.GetComponent<RectTransform>().rect.width;
        }
        
        public void SetData(TournamentPlayerData player, TournamentPlayerData opponent)
        {
            this.playerData = player;
            this.opponentData = opponent;
            playerBannedDeckId = 0;
            playerPickedDeckId = 0;
            opponentBannedDeckId = 0;
            opponentPickedDeckId = 0;
            
            playerVsWidget.SetData(player, opponent);
            deckVsWidget.SetData(player, opponent, this);
            UpdateStage(MatchMakingStage.Prepare);
        }

        public void UpdateStage(MatchMakingStage stage)
        {
            matchMakingStage = stage;
            deckVsWidget.UpdateStage(matchMakingStage);
            CloseBanAndPickWidgets();
        }
        
        public void OnStageTimeout()
        {
            switch (matchMakingStage)
            {
                case MatchMakingStage.Prepare:
                    UpdateStage(MatchMakingStage.Ban);
                    break;
                
                case MatchMakingStage.Ban:
                    UpdateStage(MatchMakingStage.BanEnd);
                    BanOpponentFirstDeckOnTimeout(); //make sure the flow can continue
                    break;
                
                case MatchMakingStage.Pick:
                    UpdateStage(MatchMakingStage.PickEnd);
                    PickFirstUnBannedDeckOnTimeout(); //make sure the flow can continue
                    break;
            }
        }

        public void OnClickCB(CompDeck compDeck, bool isPlayerDeck)
        {
            if (matchMakingStage == MatchMakingStage.BanEnd || matchMakingStage == MatchMakingStage.PickEnd || matchMakingStage == MatchMakingStage.MatchStarting)
                return;

            int deckId = compDeck.GetDeckData().ID;
            if (deckId == playerBannedDeckId || deckId == playerPickedDeckId || 
                deckId == opponentBannedDeckId || deckId == opponentPickedDeckId)
                return;
            
            var deckData = compDeck.GetDeckData();
            deckCardListWidget.SetData(deckData);
            deckCardListWidget.gameObject.SetActive(true);
            playerVsWidget.gameObject.SetActive(false);
            
            switch (matchMakingStage)
            {
                case MatchMakingStage.Ban:
                    if (!isPlayerDeck && opponentBannedDeckId == 0)
                    {
                        deckBanWidget.SetData(deckData, this);
                        deckBanWidget.gameObject.SetActive(true);
                        deckVsWidget.SetRectTransformRightOffset(banWidgetWidth);
                    }
                    else
                        CloseBanAndPickWidgets();

                    break;
                
                case MatchMakingStage.Pick:
                    if (isPlayerDeck && playerPickedDeckId == 0)
                    {
                        deckPickWidget.SetData(deckData, this);
                        deckPickWidget.gameObject.SetActive(true);
                        deckVsWidget.SetRectTransformRightOffset(banWidgetWidth);
                    }
                    else
                        CloseBanAndPickWidgets();

                    break;
            }
        }
        
        public void OnClickedBg()
        {
            CloseBanAndPickWidgets();
        }

        public void OnClickExitLobby()
        {
            StartCoroutine(NetworkHandler.Instance.PostWebRequest("https://localhost/lobby/exit", null, null, null));
            CloseWindow();
            UIManager.Instance.OpenWindow(WindowType.SelectDeckWindow, null);
        }

        private void CloseBanAndPickWidgets()
        {
            deckBanWidget.gameObject.SetActive(false);
            deckPickWidget.gameObject.SetActive(false);
            deckVsWidget.SetRectTransformRightOffset(0);
            deckCardListWidget.gameObject.SetActive(false);
            playerVsWidget.gameObject.SetActive(true);
        }

        #region Ban
        public void Ban(int deckId, bool isPlayerDeck, bool ignoreStageCheck=false)
        {
            if (isPlayerDeck)
                CloseBanAndPickWidgets();

            if (!ignoreStageCheck && matchMakingStage != MatchMakingStage.Ban)
                return;

            if (isPlayerDeck)
            {
                if (playerBannedDeckId > 0)
                    return;
                playerBannedDeckId = deckId;
                deckVsWidget.OnDeckBanned(deckId, isPlayerDeck);
                OnBanUpdated();
            }
            else
            {
                if (opponentBannedDeckId > 0)
                    return;
                
                //since url not exists, both error and success cb to OnBanSuccess, ignore the Curl error 28: Connection timed out after 1000 milliseconds 
                StartCoroutine(NetworkHandler.Instance.PostWebRequest("https://localhost/lobby/ban", new Dictionary<string, string> {
                    {"deckId", deckId.ToString()}
                }, data => OnBanSuccess(data, deckId), data => OnBanSuccess(data, deckId)));
            }
        }

        private void BanOpponentFirstDeckOnTimeout()
        {
            if (opponentBannedDeckId == 0)
                Ban(opponentData.GetDecks()[0].ID, false, true);

            if (playerBannedDeckId == 0)
                Ban(playerData.GetDecks()[0].ID, true, true);
        }
        
        private void OnBanSuccess(string data, int deckId)
        {
            // prevent concurrent issue
            if (opponentBannedDeckId > 0)
                return;
            
            opponentBannedDeckId = deckId;
            deckVsWidget.OnDeckBanned(deckId, false);
            OnBanUpdated();
            
            if (playerBannedDeckId == 0 && (matchMakingStage == MatchMakingStage.Ban || matchMakingStage == MatchMakingStage.BanEnd))
                Ban(playerData.GetDecks()[0].ID, true);
        }

        private void OnBanUpdated()
        {
            if (playerBannedDeckId > 0 && opponentBannedDeckId > 0)
                UpdateStage(MatchMakingStage.Pick);
        }
        #endregion

        #region Pick
        public void Pick(int deckId, bool isPlayerDeck,  bool ignoreStageCheck=false)
        {
            if (isPlayerDeck)
                CloseBanAndPickWidgets();

            if (!ignoreStageCheck && matchMakingStage != MatchMakingStage.Pick)
                return;
            
            if (isPlayerDeck)
            {
                if (playerPickedDeckId > 0 || playerBannedDeckId == deckId)
                    return;
                
                //since url not exists, both error and success cb to OnPickSuccess, ignore the Curl error 28: Connection timed out after 1000 milliseconds 
                StartCoroutine(NetworkHandler.Instance.PostWebRequest("https://localhost/lobby/pick", new Dictionary<string, string> {
                    {"deckId", deckId.ToString()}
                }, data => OnPickSuccess(data, deckId), data => OnPickSuccess(data, deckId)));
            }
            else
            {
                if (opponentPickedDeckId > 0)
                    return;
                opponentPickedDeckId = deckId;
                
                opponentData.Status = MatchMakingStatus.Ready;
                playerVsWidget.GetOpponentProfile().UpdateStatus(opponentData.Status);
                playerVsWidget.GetOpponentProfile().SetRegion(PlayerDeckManager.Instance.GetOpponentDeckById(opponentPickedDeckId));
                
                deckVsWidget.OnDeckPicked(deckId, isPlayerDeck);
                OnPickUpdate();
            }
        }
        
        private void PickFirstUnBannedDeckOnTimeout()
        {
            if (opponentPickedDeckId == 0)
                PickFirstUnbannedDeck(false);

            if (playerPickedDeckId == 0)
                PickFirstUnbannedDeck(true);
        }

        private void PickFirstUnbannedDeck( bool isPlayerDeck)
        {
            int excludeId = isPlayerDeck ? playerBannedDeckId : opponentBannedDeckId;
            var decks = (isPlayerDeck ? playerData : opponentData).GetDecks();
            foreach (var deckData in decks)
            {
                if (deckData.ID == excludeId)
                    continue;
                Pick(deckData.ID, isPlayerDeck, true);
                break;
            }
        }
        
        private void OnPickSuccess(string data, int deckId)
        {
            // prevent concurrent issue
            if (playerPickedDeckId > 0)
                return;
            
            playerPickedDeckId = deckId;
            
            playerData.Status = MatchMakingStatus.Ready;
            playerVsWidget.GetPlayerProfile().UpdateStatus(playerData.Status);
            playerVsWidget.GetPlayerProfile().SetRegion(PlayerDeckManager.Instance.GetPlayerDeckById(playerPickedDeckId));
            
            deckVsWidget.OnDeckPicked(deckId, true);
            OnPickUpdate();

            if (matchMakingStage != MatchMakingStage.Pick && matchMakingStage != MatchMakingStage.PickEnd) 
                return;
            
            if (opponentPickedDeckId == 0)
                PickFirstUnbannedDeck(false); //pick for opponent
        }

        private void OnPickUpdate()
        {
            if (playerPickedDeckId > 0 && opponentPickedDeckId > 0)
                UpdateStage(MatchMakingStage.MatchStarting);
        }
        #endregion
    }
}