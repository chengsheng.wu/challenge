﻿using System.Collections.Generic;
using System.Linq;
using PlayerData;
using PlayerData.Tournament;
using UI.Widgets.SelectDeck;

namespace UI.Windows.Common
{
    public class SelectDeckWindow : WindowBase
    {
        public DeckSelectionWidget deckSelectionWidget;
        public DeckConfirmWidget deckConfirmWidget;

        private List<DeckData> selectedDecks;
        
        public override void Display()
        {
            base.Display();
            deckSelectionWidget.SetData(this);
            deckSelectionWidget.gameObject.SetActive(true);
            deckConfirmWidget.gameObject.SetActive(false);
        }

        public void OnSelectionFinished(List<DeckData> selectedDecks)
        {
            this.selectedDecks = selectedDecks;
            deckSelectionWidget.gameObject.SetActive(false);
            deckSelectionWidget.ResetSelection();
            deckConfirmWidget.SetData(this, selectedDecks);
            deckConfirmWidget.gameObject.SetActive(true);
        }

        public void OnBackSelection()
        {
            //since player decks no change, no need to repopulate decks
            deckSelectionWidget.gameObject.SetActive(true);
            deckConfirmWidget.gameObject.SetActive(false);
        }

        public void OnConfirmSelection()
        {
            UIManager.Instance.OpenWindow(WindowType.TournamentBanPickWindow, windowBase =>
            {
                CloseWindow();
                (windowBase as TournamentBanPickWindow)?.SetData(
                    new TournamentPlayerData(PlayerInfoManager.Instance.GetPlayerData(), selectedDecks), 
                    new TournamentPlayerData(PlayerInfoManager.Instance.GetOpponentPlayerData(), PlayerDeckManager.Instance.GetOpponentDeckList()));
            });
        }
    }
}