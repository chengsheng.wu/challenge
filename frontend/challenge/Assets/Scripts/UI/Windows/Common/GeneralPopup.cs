﻿using System;
using Managers;
using TMPro;
using UnityEngine;

namespace UI.Windows.Common
{
    public class GeneralPopup : WindowBase
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI messageText;
        [SerializeField] private TextMeshProUGUI leftBtnText;
        [SerializeField] private TextMeshProUGUI rightBtnText;
        [SerializeField] private GameObject leftBtn;

        private Action leftBtnCB;
        private Action rightBtnCB;

        public void SetData(string title, string message, bool showLeftBtn, string leftBtnTxt, string rightBtnTxt,
            Action leftBtnCB, Action rightBtnCB)
        {
            titleText.text = !string.IsNullOrEmpty(title)
                ? title
                : LocalizationManager.Instance.GetLocalizedText("notice_header");
            messageText.text = !string.IsNullOrEmpty(message) ? message : "";

            this.leftBtnCB = leftBtnCB;
            this.rightBtnCB = rightBtnCB;

            leftBtn.SetActive(showLeftBtn);
            this.leftBtnText.text = leftBtnTxt;
            this.rightBtnText.text = rightBtnTxt;
        }

        public void OnClick_LeftBtn()
        {
            CloseWindow();
            this.leftBtnCB?.Invoke();
            leftBtnCB = null;
            rightBtnCB = null;
        }

        public void OnClick_RightBtn()
        {
            CloseWindow();
            this.rightBtnCB?.Invoke();
            leftBtnCB = null;
            rightBtnCB = null;
        }
    }
}