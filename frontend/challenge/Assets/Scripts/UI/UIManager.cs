﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Managers;
using UnityEngine;

namespace UI
{
    public class LoadingWindowData
    {
        public readonly WindowType windowType;
        public readonly Action<WindowBase> callback;

        public LoadingWindowData(WindowType windowType, Action<WindowBase> callback)
        {
            this.windowType = windowType;
            this.callback = callback;
        }
    }
    
    public class UIManager : MonoSingletonBase<UIManager>
    {
        public Transform rootNormalWindowLayer;
        public Transform rootHudWindowLayer;
        public Transform rootPopupWindowLayer;
        public Transform rootNotificationWindowLayer;

        private readonly Dictionary<WindowType, WindowBase> activeWindows = new Dictionary<WindowType, WindowBase>();
        private readonly Dictionary<WindowType, WindowBase> cachedWindows = new Dictionary<WindowType, WindowBase>();
        private readonly Dictionary<WindowType, bool> pendingCloseWindows = new Dictionary<WindowType, bool>();
        private readonly Queue<LoadingWindowData> loadingWindowQueue = new Queue<LoadingWindowData>();
        private WindowType loadingWindowType = WindowType.None;
        
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
        
        public WindowBase GetWindow(WindowType windowType)
        {
            if (cachedWindows.TryGetValue(windowType, out var window))
            {
                return window;
            }

            return null;
        }

        public bool IsWindowActive(WindowType windowType)
        {
            return activeWindows.ContainsKey(windowType);
        }
        
        public void OpenWindow(WindowType windowType, Action<WindowBase> callback)
        {
            OpenWindow(new LoadingWindowData(windowType, callback));
        }

        public void OpenWindow(LoadingWindowData loadingWindowData)
        {
            WindowType windowType = loadingWindowData.windowType;
            bool isLoadingWindow = IsLoadingWindow();
            var cachedWindow = GetWindow(windowType);
            
            if (pendingCloseWindows.ContainsKey(windowType))
                pendingCloseWindows.Remove(windowType);
            else if (isLoadingWindow)
                EnqueueLoadingWindow(loadingWindowData);
            else if (cachedWindow == null)
                LoadWindowAsync(loadingWindowData, false);
            else
                InternalOpenWindow(cachedWindow, loadingWindowData.callback);
            
            if (!isLoadingWindow)
                OpenNextWindowInLoadingQueue();
        }
        
        public void CloseWindow(WindowType windowType)
        {
            var cachedWindow = GetWindow(windowType);
            if (cachedWindow == null)
            {
                if (pendingCloseWindows.ContainsKey(windowType))
                    return;

                if (IsWindowLoadingOrInQueue(windowType))
                    pendingCloseWindows[windowType] = true;
            }
            else if (IsWindowActive(windowType))
            {
                InternalCloseWindow(cachedWindow);
            }
        }

        public void CloseAllWindowByShowMode(WindowShowMode windowShowMode)
        {
            List<WindowType> windowTypesToClose = activeWindows.Where(kvp => kvp.Value.windowShowMode == windowShowMode)
                .Select(kvp => kvp.Key).ToList();
            foreach (var windowType in windowTypesToClose)
            {
                CloseWindow(windowType);
            }
        }
        
        private void InternalOpenWindow(WindowBase windowBase, Action<WindowBase> callback)
        {
            windowBase.transform.SetAsLastSibling(); //put top on same layer.
            windowBase.Display();
            activeWindows[windowBase.windowType] = windowBase;
            callback?.Invoke(windowBase);
        }

        private void InternalCloseWindow(WindowBase windowBase)
        {
            windowBase.Hide();
            activeWindows.Remove(windowBase.windowType);
        }
        
        private void OpenNextWindowInLoadingQueue()
        {
            if (loadingWindowQueue.Count > 0)
            {
                OpenWindow(loadingWindowQueue.Dequeue());
            }
        }

        private void EnqueueLoadingWindow(LoadingWindowData loadingWindowData)
        {
            if (IsWindowLoadingOrInQueue(loadingWindowData.windowType))
                return;
            
            loadingWindowQueue.Enqueue(loadingWindowData);
        }

        private bool IsWindowLoadingOrInQueue(WindowType windowType)
        {
            return windowType == loadingWindowType || IsWindowInLoadingQueue(windowType);
        }

        private bool IsWindowInLoadingQueue(WindowType windowType)
        {
            return loadingWindowQueue.Any(data => data.windowType == windowType);
        }

        private bool IsLoadingWindow()
        {
            return loadingWindowType != WindowType.None;
        }

        private void LoadWindowAsync(LoadingWindowData loadingWindowData, bool allowMultipleLoading)
        {
            WindowType windowType = loadingWindowData.windowType;
            var cachedWindow = GetWindow(windowType);
            if (cachedWindow != null)
            {
                OpenWindow(loadingWindowData);
                return;
            }

            if (IsLoadingWindow() && !allowMultipleLoading)
                return;

            loadingWindowType = windowType;

            ResourceManager.Instance.InstantiateGameObjectAsync(
                string.Format("UIPrefabs/{0}.prefab", windowType.ToString()),
                null, Vector3.zero, windowObj =>
                {
                    var windowBase = windowObj.GetComponent<WindowBase>();
                    windowObj.SetActive(false);
                    PutWindowToCorrectLayer(windowBase);
                    cachedWindows[windowType] = windowBase;
                    loadingWindowType = WindowType.None;
                    OpenWindow(loadingWindowData);
                });
        }

        private void PutWindowToCorrectLayer(WindowBase windowBase)
        {
            Transform parent = null;
            switch (windowBase.windowLayer)
            {
                case WindowLayer.Normal:
                    parent = rootNormalWindowLayer;
                    break;
                
                case WindowLayer.Hud:
                    parent = rootHudWindowLayer;
                    break;
                
                case WindowLayer.Popup:
                    parent = rootPopupWindowLayer;
                    break;
                
                case WindowLayer.Notification:
                    parent = rootNotificationWindowLayer;
                    break;
            }

            if (parent != null)
            {
                windowBase.transform.SetParent(parent, false);
                windowBase.transform.SetAsLastSibling(); //put top on same layer.
            }
        }
    }
}
