﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof (TextMeshProUGUI))]
public class FPSCounter : MonoBehaviour
{
    const float FPSMeasurePeriod = 0.5f;
    private int mFpsAccumulator;
    private float mFpsNextPeriod;
    private int mCurrentFps;
    const string Display = "{0} FPS";
    private TextMeshProUGUI mText;

    private void Start()
    {
        mFpsNextPeriod = Time.realtimeSinceStartup + FPSMeasurePeriod;
        mText = GetComponent<TextMeshProUGUI>();
    }
    
    private void Update()
    {
        // measure average frames per second
        mFpsAccumulator++;
        if (Time.realtimeSinceStartup > mFpsNextPeriod)
        {
            mCurrentFps = (int) (mFpsAccumulator/FPSMeasurePeriod);
            mFpsAccumulator = 0;
            mFpsNextPeriod += FPSMeasurePeriod;
            mText.text = string.Format(Display, mCurrentFps);
        }
    }
}