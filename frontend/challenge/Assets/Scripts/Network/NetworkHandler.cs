﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
    public class NetworkHandler : SingletonBase<NetworkHandler>
    {
        //since url not exists, call success back directly
        public IEnumerator PostWebRequest(string url, Dictionary<string, string> data, 
            Action<string> success, Action<string> error)
        {
            success?.Invoke("");
            yield break;
//             var wwwForm = new WWWForm();
//             if (data != null)
//             {
//                 foreach (var kvp in data)
//                     wwwForm.AddField(kvp.Key, kvp.Value);
//             }
//
//             var webRequest = UnityWebRequest.Post(url, wwwForm);
//             webRequest.timeout = 5;
//             yield return webRequest.SendWebRequest();
//
// #pragma warning disable 618
//             if (webRequest.isNetworkError || webRequest.isHttpError)
// #pragma warning restore 618
//                 error?.Invoke(webRequest.error);
//             else
//                 success?.Invoke(webRequest.downloadHandler.text);
//             
//             webRequest.Dispose();
        }

        //since url not exists, call success back directly
        public IEnumerator GetWebRequest(string url, Action<string> success, Action<string> error)
        {
            success(""); 
            yield break;
//             var webRequest = UnityWebRequest.Get(url);
//             webRequest.timeout = 5;
//             yield return webRequest.SendWebRequest();
//
// #pragma warning disable 618
//             if (webRequest.isNetworkError || webRequest.isHttpError)
// #pragma warning restore 618
//                 error(webRequest.error);
//             else
//                 success(webRequest.downloadHandler.text);
//             
//             webRequest.Dispose();
        }
    }
}