﻿using System;
using System.Collections.Generic;
using Localization;
using Managers;

namespace GameData
{
    [Serializable]
    public class ConstantInfoContainer : GameDataContainer<KeyPairValue>
    {
        public override void Load(GameDataManager gameDataManager)
        {
            gameDataManager.constantInfos.Clear();
            var constantInfos = new Dictionary<string, string>();
            foreach (var element in data)
            {
                constantInfos[element.key] = element.value;
            }

            gameDataManager.constantInfos = constantInfos;
        }
    }
}