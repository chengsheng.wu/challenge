﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using CsvHelper;
using GameData;
using Localization;
using UnityEngine;

public class GameDataToScriptableObject
{
    private static string ROOT_PATH = "Assets/GameData/";
    private static readonly string PATH_LOCALIZATION_CSV = ROOT_PATH + "Csv";
    private static readonly string PATH_LOCALIZATION_DATA = ROOT_PATH + "Data";
    private static readonly Dictionary<string, Type[]> csvToDataClass = new Dictionary<string, Type[]>()
    {
        {"card_info", new []{typeof(CardInfoContainer), typeof(CardInfo)}},
        {"constant_info", new []{typeof(ConstantInfoContainer), typeof(KeyPairValue)}},
    };

    [MenuItem("Tools/GameData/Build GameData Containers")]
    public static void BuildGameDataScriptableData()
    {
        if (EditorUtility.DisplayDialog(
            "Data Overwrite Warning",
            "Are you sure you want to continue ? This will delete all existing created container data and recreate",
            "Yes", "No"))
        {
            string[] files = GetAllCsvFilesAtPath(PATH_LOCALIZATION_CSV);
            var totalProgressCount = files.Length; // for progress bar display
            var currentProgressCount = 1; // for progress bar display
            string progressStr = "{0}/{1} [{2}]";
            string fileName = "";
            Type[] dataClasses;

            DeleteDataInFolder(PATH_LOCALIZATION_DATA);
            foreach (var file in files)
            {
                string[] spl = file.Split('.')[0].Split('\\'); //trim .csv then split by '\'
                fileName = spl[spl.Length - 1]; //get the name part
                
                EditorUtility.DisplayProgressBar(
                    "Building Scriptable Object",
                    string.Format(progressStr, currentProgressCount, totalProgressCount, fileName),
                    (float)currentProgressCount/totalProgressCount);

                if (!csvToDataClass.TryGetValue(fileName, out dataClasses))
                {
                    LogUtil.ErrorFormat("Please add config of {0} to GameDataToScriptableObject.csvToDataClass!!!", fileName);
                    EditorUtility.ClearProgressBar();
                    return;
                }
                
                //load csv as container
                var container = ScriptableObject.CreateInstance(dataClasses[0]);
                ParseCsv(file, container as IGameDataContainer, dataClasses[1]);
                container.name = fileName;
                CreateContainerAssetsAtPath(PATH_LOCALIZATION_DATA, container);
                
                currentProgressCount++;       
            }

            //save created assets
            AssetDatabase.SaveAssets();
      
            //post creating handling
            EditorUtility.ClearProgressBar();                
            AssetDatabase.Refresh();
            LogUtil.WarningFormat("GameData generated success");
        }
    }

    public static bool ValidateCsv()
    {
        return true;
    }
    
    [MenuItem("Tools/GameData/Clear Game Data Containers")]
    public static void ClearAllGameDataContainers()
    {
        string[] files = Directory.GetFiles(PATH_LOCALIZATION_DATA);
        int totalProgressCount = files.Length; // for progress bar display
        int currentProgressCount = 1; // for progress bar display
        foreach (string file in files)
        {
            EditorUtility.DisplayProgressBar(
                "Cleaning Up Container Data",
                "Cleaning Up..." + file,
                (float)currentProgressCount/ totalProgressCount);
            
            AssetDatabase.DeleteAsset(file);
            currentProgressCount++;
        }

        EditorUtility.ClearProgressBar();
    }

    static void DeleteDataInFolder(string folder)
    {
        if (Directory.Exists(folder))
        {
            //delete all files under directory
            string[] currentFiles = Directory.GetFiles(folder, "*.asset");
            foreach (var aFile in currentFiles)
            {
                LogUtil.Info("delete file:" + aFile);
                File.Delete(aFile);
            }
        }
    }
    
    static string[] GetAllCsvFilesAtPath(string path)
    {
        return Directory.GetFiles(path, "*.csv");
    }

    static void CreateContainerAssetsAtPath(string savePath, ScriptableObject toSave)
    {
        AssetDatabase.CreateAsset(toSave,  string.Format("{0}/{1}.asset", savePath, toSave.name));         
    }
    
    static void ParseCsv(string path, IGameDataContainer container, Type type)
    {
        // read in the csv
        StreamReader streamReader = new StreamReader(path, System.Text.Encoding.UTF8);
        string csvText = streamReader.ReadToEnd();
        streamReader.Close();
        
        using (StringReader csvReader = new StringReader(csvText)) 
        {
            using (var reader = new CsvReader(csvReader))
            {
                var records = reader.GetRecords(type);
                container.Process(records);
            }
        }
    }
}