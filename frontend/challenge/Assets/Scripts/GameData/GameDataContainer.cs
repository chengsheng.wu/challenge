﻿using System.Collections.Generic;
using System.Linq;
using Managers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameData
{
    interface IGameDataContainer
    {
        void Process(IEnumerable<object> rawData);
        void Load(GameDataManager gameDataManager);
    }
    
    public abstract class GameDataContainer<T> : ScriptableObject, IGameDataContainer
    {
        [ListDrawerSettings(HideAddButton = true, HideRemoveButton = true, ShowPaging = false)]
        public List<T> data;

        public void Process(IEnumerable<object> rawData)
        {
            data = rawData.OfType<T>().ToList();
        }

        public virtual void Load(GameDataManager gameDataManager)
        {
        }
    }
}