﻿using System;
using System.Collections.Generic;
using Managers;

namespace GameData
{
    public enum CardRegion
    {
        Unknown,
        BandleCity,
        Bilgewater,
        Demacia,
        Freljord,
        Ionia,
        Noxus,
        PiltoverZaun,
        ShadowIsles,
        Shurima,
        Targon,
    }
    
    [Serializable]
    public class CardInfoContainer : GameDataContainer<CardInfo>
    {
        public override void Load(GameDataManager gameDataManager)
        {
            gameDataManager.cardInfos.Clear();
            var cardInfos = new Dictionary<int, CardInfo>();
            foreach (var element in data)
            {
                cardInfos[element.id] = element;
            }

            gameDataManager.cardInfos = cardInfos;
        }
    }

    [Serializable]
    public class CardInfo
    {
        public int id;
        public string name;
        public string desc;
        public string icon;
        public byte energe_cost;
        public CardRegion region;
        public bool hero; //determine whether card is a hero card.

        public CardInfo(int id, string name, string desc, string icon, byte energe_cost, CardRegion region, bool hero)
        {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.icon = icon;
            this.energe_cost = energe_cost;
            this.region = region;
            this.hero = hero;
        }
    }
}