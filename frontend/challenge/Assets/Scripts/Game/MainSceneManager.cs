﻿using Common;
using UI;

namespace Game
{
    public class MainSceneManager : MonoSingletonBase<MainSceneManager>
    {
        private void Start()
        {
            UIManager.Instance.OpenWindow(WindowType.SelectDeckWindow, null);
        }
    }
}