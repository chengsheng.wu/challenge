# Challenge

## Description
The flow begins from selecting 3 decks to join a matchmaking queue. Once the match is made,
the player is brought to the lobby and the opponent’s deck is presented. The player then looks at the
opponent’s decks, and bans one of them within 30 seconds. Once both players have banned a deck, the
UI will show which deck has been banned. The player then has 30 seconds to pick one of their own decks
to be used in the game. Once both players have chosen a deck, a game will begin

## Installation
1. Clone repository from https://gitlab.com/chengsheng.wu/challenge.git
2. Highly recommend install [Unity Hub](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe?_ga=2.228464349.1530446820.1635574458-587544960.1604396369) first to help you manage different unity versions
3. Frontend use [Unity2020.3.13f1](unityhub://2020.3.13f1/71691879b7f5), you can go to https://unity3d.com/get-unity/download/archive to find archived versions, tap `Unity 2020.x` and scroll down to `Unity 2020.3.13`, click `Unity Hub` button to install via Unity Hub.
4. In `Unity Hub` main menu, Click `ADD` button then go to `{repoRoot}/frontend/`, select `challenge` and click `Select Folder`, this is the unity project root folder. 
5. After you finish step 4, a new entry will appear in `Unity Hub`, set following and click the `challenge` to open the project
    - Unity Version: `2020.3.13f1`, 
    - Target Platform: `PC`   
6. After unity loading finish, verify `unity version` and `platform` on Top banner
7. This project use IDE `JetBrain Rider 2020.2.4` for development, but you can use other `C# IDE` as well.
8. Open `Preload` scene in Assets/Scenes/ and Play

## Plugins
- [DOTween](https://assetstore.unity.com/packages/tools/visual-scripting/dotween-pro-32416): A powerful `animation engine` that provide lots of APIs to help you animate objects via script. 
- [Odin Inspector](https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041): A `Unity Editor tool` makes editor more user-friendly 
- [Rainbow Folder](https://assetstore.unity.com/packages/tools/utilities/rainbow-folders-2-143526): A `Unity Editor tool` helps you customize icon and background of your project folders

## Unity Packages:
- [Addressables](https://docs.unity3d.com/Packages/com.unity.addressables@1.19/manual/index.html): Provides tools and scripts to `organize and package assets` for your application and an API to load and release assets at runtime 
- [Device Simulator](https://docs.unity3d.com/Packages/com.unity.device-simulator@2.2/manual/index.html): A `Unity Editor tool` gives an accurate preview of how an app will look on different devices.
- [TextMeshPro](https://docs.unity3d.com/Packages/com.unity.textmeshpro@3.0/manual/index.html): An advanced `text solution` over Unity Text. It wins almost every aspect over Unity Text except it doesn't support fallback to system font. You need to create static or dynamic FontAsset to make it work.

## Code Explanation
- All screens are implemented and tested include Back Selection and Exit Lobby. If you find any problem, please let me know.
- Used default resolution 1920x1080 for landscape and set canvas scaler to match height
- `GameStarter` is the entrance when you click Play, it calls `GameManager` to StartGame
- `GamManager` does all the initializations.
- `PatchManager` uses Addressables to check and patch.
- `LocalizationManager` loads localization for splashscreen and also for game. You can refer [Localization README](./frontend/challenge/Assets/Localizations/README.md) for details.
- `GameDataManager` loads all game data into individual maps, for example cardInfos. The flow is 
    - Designers work on Excel
    - Export csv and put under Assets/GameData/Csv
    - Run `Tools/GameData/Build GameData Container` and it will generate runtime used data in `Assets/GameData/Data/`
    - Each csv will translate to one container which is a ScriptableObject, you can refer Scripts/GameData/ for details.
    - If you want to change card_info.csv or constant_info.csv, verify they are saved using ',' as delimeter and run `Tools/GameData/Build GameData Container` to regen data.
- `ResourceManager` preloads and cache prefabs and sprites. It also provides some api for aysnc asset load
- `SceneManager` does scene transition, show loading window when change scene.
- `NetworkHandler` proived post and get, due to urls are fake, I just callback success. 
- `UIManager` provides apis for window open, close, load, cache and status. It works together with WindowType, WindowBase etc. They are in Scripts/UI/
    - `SelectDeckWindow` handles Screenshot1 and Screenshot2. It further split to DeckSelectionWidget and DeckConfirmWidget, the scripts are under Scripts/UI/Widgets/SelectDeck/
    while prefabs are under Prefabs/UI/Widgets/SelectDeck/
    - `TournamentBanPickWindow` handles Screenshot3 - 7 the folder structure is same as above
    - `Components` mainly for common usage, script are all under Scripts/UI/Components, prefabs under Prefabs/UI/Common/Components/
    - There are also common `WindowBase` prefabs under UI/Common/BaseWindows/ and common `Buttons` under UI/Common/Buttons/
    - `Widgets` in current structure are normally used as part of specific window, in a same feature they may still reusable. 
    For example: DeckAndHeroWidget.prefab are used both on DeckBanWidget and DeckPickWidget
- `PlayerDeckManager` and `PlayerInfoManager` contains all the mockup data for player and opponent
- In Comp_CardInList there is a heroTag, will show when card is a hero card. Not looks nice, but just to provide same logic as LoR.

## Further Improvement
- Can create unit test for game data validate, due to time constraint I only added a Unit test for Localization DuplicateKey check.
- For asset integration, screenshots only provide layout, information like size and position are missed, can upload mockup to 3rd Party Tool like 
Zeplin/Figma so that programmer will be easy to create layout in unity. It takes me some time to adjust size and positions to make ui look nice.
- Lack of assets, so ui images just show different colors which make it look ugly.
- After integrate with server, I can foresee some place may need to change a bit, 
for example server will not return immediately and may need a blocker to prevent user click or add more checks to handle edge case, 
currently I checked as much as possible but may still missing some place.
- When you see a text display start with # means it's not localized yet. Due to Time constraint I didn't localize card and deck name.

## Questions Clarified with Albert
- Screenshot1: filter by region, sort by name.
- Each deck can bring max 6 champions. If a deck has more than 3 unique champions, it's displayed in 2 rows. There's no scroll view
- No deck customization is required in this project
- In LoR, each card is associated with a region, and a competitive deck may contain up to 2 regions only.  They are not interactable
- Screen6 contains some cosmetics, Albert suggested I can ignore that for now.
- Unit Test is not required, just show some knowledge is enough, current I only added one Unit test for Localization duplicate key validation.
- I can use 3rd Party plugins. 
 